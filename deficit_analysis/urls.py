from django.urls import path, include
from .views import *

urlpatterns = [
    path('get_kpis/',GetKPIs.as_view()),
    path('get_sample_deficit_summary/',SampleDefictSummary.as_view()),
    path('get_booth_summary/',BoothWiseSummary.as_view()),
]

