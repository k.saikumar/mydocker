from datetime import *
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from .models import *
from agent_response.models import *
from users.views import handle_auth_exceptions
from django.db.models import Sum, Count, F
from users.models import *
import random


class GetKPIs(APIView) :
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            dataList = []
            filter_object = Q()

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)
            
            if 'agent_id' in request.data and request.data['agent_id']:
                id_list = [id.strip() for id in request.data['agent_id'].split(',')]
                userQuery = User.objects.filter(id__in = id_list).values_list('mapped_booths', flat = True)
                filter_object &= Q(booth_number__in = set(','.join(map(str, userQuery)).split(',')))
                userQuery1 = User.objects.filter(id__in=id_list).values_list('ac_name', flat=True)
                filter_object &= Q(ac_name__in=set(','.join(map(str, userQuery1)).split(',')))

            totalQueryset = ElectoralList.objects.filter(filter_object,survey_status = 1).count()
            todayQueryset = ElectoralList.objects.filter(filter_object, survey_status= 1, 
                surveyed_timestamp__gte = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0),
                surveyed_timestamp__lt  = datetime.today().replace(hour=23, minute=59, second=59, microsecond=999999)
                ).count()
            
            totalTarget = ElectoralList.objects.filter(filter_object).count()
            todayTarget = ElectoralList.objects.filter(filter_object, 
                surveyed_timestamp__gte = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0),
                surveyed_timestamp__lt  = datetime.today().replace(hour=23, minute=59, second=59, microsecond=999999)
                ).count()
            
            dataList.append({
                'total_target' : totalTarget,
                'total_achived' : totalQueryset,
                'today_target'  : todayTarget,
                'today_achived'  : todayQueryset,
            })

            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = dataList
            return Response(result,status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)


class SampleDefictSummary(APIView):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            filter_object = Q()

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)
            
            if 'agent_id' in request.data and request.data['agent_id']:
                id_list = [id.strip() for id in request.data['agent_id'].split(',')]
                userQuery = User.objects.filter(id__in = id_list).values_list('mapped_booths', flat = True)
                filter_object &= Q(booth_number__in = set(','.join(map(str, userQuery)).split(',')))
            
            age_ranges = [
                (18, 25, '18-25'),
                (26, 35, '26-35'),
                (36, 50, '36-50'),
                (51, 100, 'Over_50'),
            ]
            gender_categories = ['male', 'female']
            data_list = []
            for _,_, age_category in age_ranges:
                for gender in gender_categories:
                    filter_object1   = Q(gender = gender, age_category = age_category)
                    target_count    = ElectoralList.objects.filter(filter_object, filter_object1).count()
                    achieved_count  = ElectoralList.objects.filter(filter_object, filter_object1, survey_status=1).count()

                    data_list.append({
                        'profile_name': f'{age_category} {gender}',
                        'target': target_count,
                        'achieved': achieved_count,
                        'deficit': target_count - achieved_count,
                    })
                
            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = data_list
            return Response(result,status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        
class BoothWiseSummary(APIView, PageNumberPagination):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            filter_object1 = Q()
            filter_object2 = Q()

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object1 &= Q(ac_name__in=ac_list)
                filter_object2 &= Q(ac_name__in=ac_list)
            
            if 'agent_id' in request.data and request.data['agent_id']:
                id_list = [id.strip() for id in request.data['agent_id'].split(',')]

                userQuery = User.objects.filter(id__in = id_list).values_list('mapped_booths', flat = True)
                filter_object2 &= Q(booth_number__in=set(','.join(map(str, userQuery)).split(',')))
                filter_object1 &= Q(booth_number__in=set(','.join(map(str, userQuery)).split(',')))

            achievedQueryset= ElectoralList.objects.filter(filter_object1, survey_status = 1).values('booth_number').annotate(count = Count('id'))
            targetQueryset= ElectoralList.objects.filter(filter_object1).values('booth_number').annotate(count = Count('id'))
            booth_count_mapping_achieved = {entry['booth_number']: entry['count'] for entry in achievedQueryset}
            booth_count_mapping_target   = {entry['booth_number']: entry['count'] for entry in targetQueryset}
            getBooth = BoothList.objects.filter(filter_object2).values('booth_number', 'booth_name').distinct()

            dataList = []
            for entry in getBooth:
                boothNumber = entry['booth_number']
                boothName   = entry['booth_name']
                dataList.append({
                    'booth'    :f'{boothName} {boothNumber}',
                    'target'   : booth_count_mapping_target.get(boothNumber, 0),
                    'achieved' : booth_count_mapping_achieved.get(boothNumber, 0),
                    'deficit'  : max(0, booth_count_mapping_target.get(boothNumber, 0) - booth_count_mapping_achieved.get(boothNumber, 0)),
                    })
            
            paginated_data = self.paginate_queryset(dataList, request, view=self)
            paginated_data = self.get_paginated_response(paginated_data).data
            
            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['next']      = paginated_data['next']
            result['result']['previous']  = paginated_data['previous']
            result['result']['count']     = paginated_data['count']
            result['result']['data']      = paginated_data['results']
            return Response(result, status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
