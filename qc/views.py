from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from survey_analytics_project import settings
import jwt
from datetime import datetime, timedelta
from django.utils import timezone
from agent_response.models import *
import json
import boto3
from botocore.exceptions import ClientError
import re
from django.db.models import F,Q,Count
from rest_framework.pagination import PageNumberPagination
from users.views import *
from users.models import *


class QC_Buckets(APIView):
    @handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}   
        try:
            user_data           =   request.user_data
            filter_object = Q()
            if user_data['assigned_agent_id'] is not None or user_data['assigned_agent_id']:
                agent_ids_list = [int(agent_id) for agent_id in user_data['assigned_agent_id'].split(',')]
                filter_object   &= Q(surveyor_id__in =agent_ids_list)
            else:
                result['status'] = "OK"
                result['valid']  = True
                result['result']['message'] = "this qc has no agents"       
                return Response(result,status=status.HTTP_200_OK)  

            if 'survey_date' in request.data and request.data['survey_date'] != '':
                date_filter = Q(created_at__date = request.data['survey_date'])
                filter_object  &= date_filter

            data=SurveyForm.objects.filter(filter_object).values('id')
            pending = data.filter(Q(qc_status= 0)).count()
            completed = data.filter(Q(qc_status = 1)).count()
        
            result1 = {}
            result1['buckets'] = [{"bucket_id": "0", "bucket_name": "Pending", "count": pending},
                                {"bucket_id": "1", "bucket_name": "Completed","count": completed},
                                ]

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = result1
            return Response(result,status=status.HTTP_200_OK)  
                   
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)
        
       
class QC_Bucket_data(APIView,PageNumberPagination):
    @handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}   
        try:
            user_data           =   request.user_data 
            bucket_id = request.data['bucket_id']
            filter_object = Q()
            if user_data['assigned_agent_id'] is not None or user_data['assigned_agent_id']:
                agent_ids_list = [int(agent_id) for agent_id in user_data['assigned_agent_id'].split(',')]
                filter_object   &= Q(surveyor_id__in =agent_ids_list)
            else:
                result['status'] = "OK"
                result['valid']  = True
                result['result']['message'] = "this qc has no agents"       
                return Response(result,status=status.HTTP_200_OK)  
            
            if 'survey_date' in request.data and request.data['survey_date'] != '':
                date_filter = Q(created_at__date = request.data['survey_date'])
                filter_object  &= date_filter
            
            filter_object   &= Q(qc_status=bucket_id)
            data=SurveyForm.objects.filter(filter_object).values().order_by('-id')
            total_count         =           len(data)
            # paginated_data = self.paginate_queryset(data , request, view=self)            
            # paginated_data= self.get_paginated_response(paginated_data).data
            result['status'] = "OK"
            result['valid']  = True
            result['total_count'] = total_count
            result['result']['message'] = "QC data is fetched successfully"
            # result['result']['next']     = paginated_data['next']
            # result['result']['previous'] = paginated_data['previous']
            # result['result']['count']    = paginated_data['count']
            # result['result']['data']     = paginated_data['results'] 

            result['result']['data']  = data
            return Response(result,status=status.HTTP_200_OK)  
                   
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)
 

class QC_Submit(APIView):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access"}

        try :
            getac = SurveyForm.objects.filter(id = request.data['response_id']).values('ac_name')
            queryset = QcSurveyForm.objects.create(
                response_id_id = request.data['response_id'],
                surveyor_id_id = request.data['surveyor_id'],
                qc_agent_id_id = request.data['qc_agent_id'],
                ac_name = getac[0]['ac_name'],
                recorded_audio = request.data['recorded_audio'],

                age = request.data['age'],
                area = request.data['area'],
                gender = request.data['gender'],
                occupation = request.data['occupation'],
                other_occupation = request.data['other_occupation'],
                education = request.data['education'],
                govt_hospital_rating = request.data['govt_hospital_rating'],
                education_facility_rating = request.data['education_facility_rating'],
                roads_rating = request.data['roads_rating'],
                electricity_rating = request.data['electricity_rating'],
                drainage_rating = request.data['drainage_rating'],
                house_voters_count = request.data['house_voters_count'],
                member_voted_2019ae = request.data['member_voted_2019ae'],
                has_voted_2019ae = request.data['has_voted_2019ae'],
                primary_consider_vote = request.data['primary_consider_vote'],
                vote_decision_2019ae = request.data['vote_decision_2019ae'],
                party_voted_for_2019ae = request.data['party_voted_for_2019ae'],
                other_party_voted_for_2019ae = request.data['other_party_voted_for_2019ae'],
                party_voted_for_2019ge = request.data['party_voted_for_2019ge'],
                other_party_voted_for_2019ge = request.data['other_party_voted_for_2019ge'],
                impt_election_issue_2024ae = request.data['impt_election_issue_2024ae'],
                party_vote_for_2024ae = request.data['party_vote_for_2024ae'],
                other_party_vote_for_2024ae = request.data['other_party_vote_for_2024ae'],
                reason_vote_2024ae = request.data['reason_vote_2024ae'],
                pref_tdp_jsp_2024ae = request.data['pref_tdp_jsp_2024ae'],
                party_vote_for_2024ge = request.data['party_vote_for_2024ge'],
                other_party_vote_for_2024ge = request.data['other_party_vote_for_2024ge'],
                next_cm_2024ae = request.data['next_cm_2024ae'],
                other_next_cm_2024ae = request.data['other_next_cm_2024ae'],
                is_satisfied_jagan_govt = request.data['is_satisfied_jagan_govt'],
                good_things_jagan_govt = request.data['good_things_jagan_govt'],
                failure_jagan_govt = request.data['failure_jagan_govt'],
                schemes_heard_jagan_govt = request.data['schemes_heard_jagan_govt'],
                any_member_scheme_benefit = request.data['any_member_scheme_benefit'],
                better_jagan_cbn = request.data['better_jagan_cbn'],
                shd_jagan_another_chance = request.data['shd_jagan_another_chance'],
                is_satisfies_mla = request.data['is_satisfies_mla'],
                shd_current_mla_ticket_again = request.data['shd_current_mla_ticket_again'],
                sure_vote_current_mla = request.data['sure_vote_current_mla'],
                party_can_win = request.data['party_can_win'],
                other_party_can_win = request.data['other_party_can_win'],
                most_populous_caste = request.data['most_populous_caste'],
                other_most_populous_caste = request.data['other_most_populous_caste'],
                caste = request.data['caste'],
                other_caste = request.data['other_caste'],
                social_category = request.data['social_category'],
                religion = request.data['religion'],
                other_religion = request.data['other_religion'],
                monthly_income_family = request.data['monthly_income_family'],
                things_possess = request.data['things_possess'],
                house_type = request.data['house_type'],
                name = request.data['name'],
                phone_number = request.data['phone_number'],
                created_at = datetime.today(),
                second_party_vote_for_2024ae = request.data ['second_party_vote_for_2024ae'],
                other_second_party_vote_for_2024ae = request.data['other_second_party_vote_for_2024ae'],

                # Feedback
                age_feedback = request.data['age_feedback'],
                area_feedback = request.data['area_feedback'],
                gender_feedback = request.data['gender_feedback'],
                occupation_feedback = request.data['occupation_feedback'],
                education_feedback = request.data['education_feedback'],
                govt_hospital_rating_feedback = request.data['govt_hospital_rating_feedback'],
                education_facility_rating_feedback = request.data['education_facility_rating_feedback'],
                roads_rating_feedback = request.data['roads_rating_feedback'],
                electricity_rating_feedback = request.data['electricity_rating_feedback'],
                drainage_rating_feedback = request.data['drainage_rating_feedback'],
                house_voters_count_feedback = request.data['house_voters_count_feedback'],
                member_voted_2019ae_feedback = request.data['member_voted_2019ae_feedback'],
                has_voted_2019ae_feedback = request.data['has_voted_2019ae_feedback'],
                primary_consider_vote_feedback = request.data['primary_consider_vote_feedback'],
                vote_decision_2019ae_feedback = request.data['vote_decision_2019ae_feedback'],
                party_voted_for_2019ae_feedback = request.data['party_voted_for_2019ae_feedback'],
                party_voted_for_2019ge_feedback = request.data['party_voted_for_2019ge_feedback'],
                impt_election_issue_2024ae_feedback = request.data['impt_election_issue_2024ae_feedback'],
                party_vote_for_2024ae_feedback = request.data['party_vote_for_2024ae_feedback'],
                reason_vote_2024ae_feedback = request.data['reason_vote_2024ae_feedback'],
                pref_tdp_jsp_2024ae_feedback = request.data['pref_tdp_jsp_2024ae_feedback'],
                party_vote_for_2024ge_feedback = request.data['party_vote_for_2024ge_feedback'],
                next_cm_2024ae_feedback = request.data['next_cm_2024ae_feedback'],
                # other_next_cm_2024ae_feedback = request.data['other_next_cm_2024ae_feedback'],
                is_satisfied_jagan_govt_feedback = request.data['is_satisfied_jagan_govt_feedback'],
                good_things_jagan_govt_feedback = request.data['good_things_jagan_govt_feedback'],
                failure_jagan_govt_feedback = request.data['failure_jagan_govt_feedback'],
                schemes_heard_jagan_govt_feedback = request.data['schemes_heard_jagan_govt_feedback'],
                any_member_scheme_benefit_feedback = request.data['any_member_scheme_benefit_feedback'],
                better_jagan_cbn_feedback = request.data['better_jagan_cbn_feedback'],
                shd_jagan_another_chance_feedback = request.data['shd_jagan_another_chance_feedback'],
                is_satisfies_mla_feedback = request.data['is_satisfies_mla_feedback'],
                shd_current_mla_ticket_again_feedback = request.data['shd_current_mla_ticket_again_feedback'],
                sure_vote_current_mla_feedback = request.data['sure_vote_current_mla_feedback'],
                party_can_win_feedback = request.data['party_can_win_feedback'],
                # other_party_can_win_feedback = request.data['other_party_can_win_feedback'],
                most_populous_caste_feedback = request.data['most_populous_caste_feedback'],
                # other_most_populous_caste_feedback = request.data['other_most_populous_caste_feedback'],
                caste_feedback = request.data['caste_feedback'],
                # other_caste_feedback = request.data['other_caste_feedback'],
                social_category_feedback = request.data['social_category_feedback'],
                religion_feedback = request.data['religion_feedback'],
                # other_religion_feedback = request.data['other_religion_feedback'],
                monthly_income_family_feedback = request.data['monthly_income_family_feedback'],
                things_possess_feedback = request.data['things_possess_feedback'],
                house_type_feedback = request.data['house_type_feedback'],
                name_feedback = request.data['name_feedback'],
                phone_number_feedback = request.data['phone_number_feedback'],
                second_party_vote_for_2024ae_feedback = request.data['second_party_vote_for_2024ae_feedback'],
            )

            
            survey_queryset = SurveyForm.objects.filter(id = request.data['response_id']).values()

            data_list = [
                'age', 'gender', 'occupation',
                'has_voted_2019ae', 
                'party_voted_for_2019ae', 'party_voted_for_2019ge', 'party_vote_for_2024ae', 'pref_tdp_jsp_2024ae', 'party_vote_for_2024ge', 
                'next_cm_2024ae', 'is_satisfied_jagan_govt', 'better_jagan_cbn', 'shd_jagan_another_chance', 'is_satisfies_mla', 
                'party_can_win', 'caste', 'social_category', 
                'religion', 'area', 'second_party_vote_for_2024ae'
            ]

            match_count = sum(
                1 for key in data_list if str(survey_queryset[0].get(key)) == str(request.data.get(key))
            )

            AgentQcResponseMatch.objects.create(
                created_at = datetime.today(),
                surveyor_id_id=request.data['surveyor_id'],
                response_id_id=request.data['response_id'],
                **{f'{key}_match': str(survey_queryset[0][key]).lower() == str(request.data[key]).lower() for key in data_list},
                match_count= match_count
            )

            qc_survey_form = QcSurveyForm.objects.filter(response_id_id=request.data['response_id']).first()
            qc_survey_form.response_id.qc_status = 1 
            qc_survey_form.response_id.save()  

            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Response QC successfully"
            return Response(result,status=status.HTTP_200_OK)

        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)