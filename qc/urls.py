from django.urls import path
from .views import *

urlpatterns = [
    # path('submit_qc_response/',Submit_QC_Response.as_view(),name="submit_qc_response"),
    # path('get_qc_response/',get_QC_Response.as_view(),name = "get_qc_response"),
    path('get_qc_buckets/',QC_Buckets.as_view(),name = "qc_buckets"),
    path('get_qc_bucket_data/',QC_Bucket_data.as_view(),name = "qc_bucket_data"),
    path('qc_submit/',QC_Submit.as_view()),

]