from datetime import *
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from django.db.models import Count, Sum
from django.db.models.functions import ExtractHour
from .models import *
import jwt
from survey_analytics_project import settings
from django.db.models.functions import TruncDate
from users.views import handle_auth_exceptions
from django.db.models import Sum, Count, F, ExpressionWrapper, fields, FloatField
from datetime import datetime
from users.models import *
import random
from notifications.models import *




def validatedate(date_text):
    try:
        datetime.strptime(date_text,'%Y-%m-%d')
        return True
    except :
        return False

def convert_to_am_pm_format(datetime_str):
    time_in_am_pm_format = datetime_str.strftime('%I %p').lstrip('0').replace(' 0', ' ')
    return time_in_am_pm_format

class Acwise_summary(APIView):
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            search_string = request.data.get('search_string')
            filter_object = Q()
            filter_object1 = Q()
            filter_object2 = Q()
            if search_string != '':
                search_condition = Q(ac_name__icontains = search_string)
                filter_object &= search_condition
                filter_object1 &= search_condition

            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            today = datetime.today().date()
            data                = SurveyForm.objects.filter(filter_object).values('ac_name').annotate(total_cf_count=Count('id'),today_cf_count = Count(id,filter=(models.Q(created_at__date=today))))
            final_response = []
            for row in data:
                temp = {}
                temp['ac_name'] = row['ac_name']
                temp['total_cf_count'] = row['total_cf_count']
                temp['today_cf_count'] = row['today_cf_count']
                userQueryset = User.objects.filter(ac_name = row['ac_name'],role=5).values_list('id', flat=True)
                
                if 'start_date' in request.data and request.data['start_date'] != '' and 'end_date' in request.data and request.data['end_date'] != '' :
                    filter_object2 &= Q(punch_time__range = (start_date_new, end_date_new))
                else :
                    filter_object2 &= Q(punch_time__date = datetime.today())
                attQueryset  = UserAttendance.objects.filter( filter_object2,
                    user_id__in =list(userQueryset), 
                    # punch_time__date = datetime.today(), 
                    punch_status = 0).values('user_id').distinct().count()
                temp['today_active_agent_count'] = attQueryset
                final_response.append(temp)

            getac = BoothList.objects.filter(filter_object1).values_list('ac_name', flat=True).distinct()
            result_list = []
            default_values = {'total_cf_count': 0, 'today_cf_count': 0, 'today_active_agent_count': 0}
            for entry in getac:
                data_entry = next((item for item in final_response if item['ac_name'] == entry), None)
                if data_entry:
                    result_list.append({
                        'ac_name': entry,
                        'total_cf_count': data_entry.get('total_cf_count', 0),
                        'today_cf_count': data_entry.get('today_cf_count', 0),
                        'today_active_agent_count': data_entry.get('today_active_agent_count', 0)
                    })
                else:
                    result_list.append({'ac_name': entry, **default_values})
            result['status']    = 'OK'
            result['valid']     =  True
            result['result']['message'] = "Ac wise summary fetched successfully."
            result['result']['data']    = result_list

            return Response(result,status = status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
     
class SummaryHourlyGraph(APIView) :
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            filter_object = Q()

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)

            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
            queryset = SurveyForm.objects.filter(filter_object).values('ac_name').annotate(count=Count('id'))
            hourly_data = {}
            queryset_qc = QcSurveyForm.objects.filter(filter_object).values('ac_name').annotate(count=Count('id'))
            getids = queryset_qc.values('response_id')

            current_date = datetime.strptime(f"{start_date} 00:00:00.000000", '%Y-%m-%d %H:%M:%S.%f').date()
            end_date = datetime.strptime(f"{end_date} 00:00:00.000000", '%Y-%m-%d %H:%M:%S.%f').date()

            hourly_data = {}

            while current_date <= end_date:
                hourly_data_for_date = {}

                for hour in range(9, 19):
                    start_time = datetime.combine(current_date, datetime.strptime(str(hour), '%H').time())
                    end_time = start_time + timedelta(hours=1)

                    entries_in_hour = queryset.filter(created_at__range=(start_time, end_time))
                    date_range = start_time, end_time
                    total_count = entries_in_hour.aggregate(Sum('count'))['count__sum'] or 0 

                    cf_cla_cnt = queryset_qc.filter(created_at__range=date_range).aggregate(Sum('count'))['count__sum'] or 0
                    cla_count = AgentQcResponseMatch.objects.filter(created_at__range=date_range, response_id__in=getids, match_count=20).count()
                    qla_count = AgentQcResponseMatch.objects.filter(created_at__range=date_range, response_id__in=getids).aggregate(hourly_sum=Sum('match_count'))['hourly_sum'] or 0

                    hourly_data_for_date[f"{convert_to_am_pm_format(start_time)} - {convert_to_am_pm_format(end_time)}"] = {
                        'CF'          : total_count,
                        'CLA'         : 0 if cf_cla_cnt == 0 else (cla_count / cf_cla_cnt) * 100,
                        'QLA'         : 0 if cf_cla_cnt == 0 else (qla_count / (cf_cla_cnt * 20)) * 100,
                        'efficiency'  : round(random.uniform(1, 3), 2),
                    }

                hourly_data[current_date.strftime('%Y-%m-%d')] = hourly_data_for_date
                current_date += timedelta(days=1)

            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = hourly_data
            result['result']['count'] = len(hourly_data)
            return Response(result,status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)

class SummaryDateWiseGraph(APIView) :
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access"}
        
        try: 
            filter_object = Q()

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)

            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
            
            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)

            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
            queryset = SurveyForm.objects.filter(filter_object).values('ac_name').annotate(count=Count('id'))
            queryset_qc = QcSurveyForm.objects.filter(filter_object).values('ac_name').annotate(count=Count('id'))
            getids = queryset_qc.values('response_id')

            current_date = datetime.strptime(start_date, '%Y-%m-%d').date()
            end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
            
            daily_data = []
            for i in range((end_date + timedelta(days=1) - current_date).days):
                date_range = (current_date + timedelta(days=i), current_date + timedelta(days=i + 1))
                
                cf_count = queryset.filter(created_at__range=date_range).aggregate(Sum('count'))['count__sum'] or 0
                cf_cla_cnt = queryset_qc.filter(created_at__range=date_range).aggregate(Sum('count'))['count__sum'] or 0
                cla_count = AgentQcResponseMatch.objects.filter(created_at__range=date_range, response_id__in=getids, match_count=20).count()
                qla_count = AgentQcResponseMatch.objects.filter(created_at__range=date_range, response_id__in=getids).aggregate(hourly_sum=Sum('match_count'))['hourly_sum'] or 0

                daily_data.append({
                    'date': date_range[0].strftime('%Y-%m-%d'),
                    'CF': cf_count,
                    'CLA': 0 if cf_cla_cnt == 0 else (cla_count / cf_cla_cnt) * 100,
                    'QLA': 0 if cf_cla_cnt == 0 else (qla_count / (cf_cla_cnt * 20)) * 100,
                    'efficiency': round(random.uniform(1, 3), 2),
                })


            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = daily_data
            result['result']['count'] = len(daily_data)
            return Response(result,status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)


class HourlySummary(APIView):
    # @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        filter_object = Q()
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            ac_filter = Q(ac_name = request.data['ac_name'])
            filter_object &= ac_filter
        
        today               =  datetime.today().date()
        diff_days           =  0
        if 'start_date' in request.data and 'end_date' in request.data and request.data['start_date'] != '' and request.data['end_date'] != '':
            start_date      = request.data['start_date']
            end_date        = request.data['end_date']
            start_date      = datetime.strptime(start_date, '%Y-%m-%d').date()
            end_date        = datetime.strptime(end_date, '%Y-%m-%d').date()
            diff_days       = (end_date - start_date).days
            # print(end_date)
            today           = end_date
            end_date        += timedelta(days=1)
            # Create a Q object for filtering
            date_filter = Q(created_at__range=(start_date, end_date))
            filter_object &= date_filter
        
        data                =   SurveyForm.objects.filter(filter_object).annotate(date=TruncDate('created_at')).values('date').annotate(total_cf_count=Count('id')).order_by('-total_cf_count')
        
        yesterday           =   today - timedelta(days=1) if diff_days > 0 else today
        bestday             =   today
        if len(data) > 0:
            bestday = data[0]['date']
            

        final_response      =   {}
        final_response['best_day']  = bestday
        final_response['today']  = today
        final_response['previous_day']  = yesterday
        total_cf_best_day = [entry['total_cf_count'] for entry in data if entry['date'] == bestday]
        total_cf_best_day_count = (total_cf_best_day[0] if len(total_cf_best_day) > 0 else 0) if len(data) > 0 else 0 
        total_cf_yesterday = [entry['total_cf_count'] for entry in data if entry['date'] == yesterday]
        total_cf_yesterday_count = (total_cf_yesterday[0] if len(total_cf_yesterday) > 0 else 0)if len(data) > 0 else 0  
        total_cf_today = [entry['total_cf_count'] for entry in data if entry['date'] == today]
        total_cf_today_count = (total_cf_today[0] if len(total_cf_today) > 0 else 0)if len(data) > 0 else 0  
        best_day_hourly_data = get_hourly_data(bestday,request.data['ac_name'])
        today_hourly_data    = get_hourly_data(today,request.data['ac_name'])
        yesterday_hourly_data = get_hourly_data(yesterday,request.data['ac_name'])
        best_day_attendance     = get_attendance(bestday,request.data['ac_name'])
        today_attendance        = get_attendance(today,request.data['ac_name'])
        yesterday_attendance    = get_attendance(yesterday,request.data['ac_name'])
        summary_table_data = []
        for i in range(9,20):
            row_wise_response = {}
            if i <= 10:
                row_wise_response['Hours'] = str(i)+'AM - '+str(i+1)+'AM'
            elif i >=13:
                row_wise_response['Hours'] = str(i-12)+'PM - '+str(i-11)+'PM'
            elif i == 11:
                row_wise_response['Hours'] = str(i)+'AM - '+str(i+1)+'PM'
            else:
                row_wise_response['Hours'] = str(i)+'PM - '+str(1)+'PM'
            row_wise_response['today_CF_Count'] = next((entry['hourly_count'] for entry in today_hourly_data if entry['hour_of_day'] == i), 0)
            row_wise_response['today_CF_Avg'] = round((next((entry['hourly_count'] for entry in today_hourly_data if entry['hour_of_day'] == i), 0)/today_attendance),2) if today_attendance > 0 else (next((entry['hourly_count'] for entry in today_hourly_data if entry['hour_of_day'] == i), 0))
            row_wise_response['previous_CF_Count'] = next((entry['hourly_count'] for entry in yesterday_hourly_data if entry['hour_of_day'] == i), 0)
            row_wise_response['previous_CF_Avg'] = round((next((entry['hourly_count'] for entry in yesterday_hourly_data if entry['hour_of_day'] == i), 0)/yesterday_attendance),2) if yesterday_attendance > 0 else (next((entry['hourly_count'] for entry in yesterday_hourly_data if entry['hour_of_day'] == i), 0))
            row_wise_response['bestday_CF_Count'] = next((entry['hourly_count'] for entry in best_day_hourly_data if entry['hour_of_day'] == i), 0)
            row_wise_response['bestday_CF_Avg'] = round((next((entry['hourly_count'] for entry in best_day_hourly_data if entry['hour_of_day'] == i), 0)/best_day_attendance),2) if best_day_attendance > 0 else (next((entry['hourly_count'] for entry in best_day_hourly_data if entry['hour_of_day'] == i), 0))
            summary_table_data.append(row_wise_response)
        
        total_row = {}
        total_row['Hours'] = 'Total'
        total_row['today_CF_Count'] = total_cf_today_count
        total_row['today_CF_Avg'] = round((total_cf_today_count/today_attendance),2) if today_attendance > 0 else total_cf_today_count
        total_row['previous_CF_Count'] = total_cf_yesterday_count
        total_row['previous_CF_Avg'] = round((total_cf_yesterday_count/yesterday_attendance),2) if yesterday_attendance > 0 else total_cf_yesterday_count
        total_row['bestday_CF_Count'] = total_cf_best_day_count
        total_row['bestday_CF_Avg'] = (total_cf_best_day_count/best_day_attendance) if best_day_attendance > 0 else total_cf_best_day_count
        summary_table_data.append(total_row)


        final_response['summaryTableData'] = summary_table_data
        result['status']    = 'OK'
        result['valid']     =  True
        result['result']['message'] = "Hourly summary data fetched successfully."
        result['result']['data']    = final_response
        return Response(result,status = status.HTTP_200_OK)
        

def get_hourly_data(input_date,ac_name):
    if ac_name != '':
        hourly_data         = SurveyForm.objects.filter(ac_name=ac_name,created_at__date = input_date).annotate(hour_of_day=ExtractHour('created_at')).values('hour_of_day').annotate(hourly_count=Count('id')).order_by('hour_of_day')
    else:
        hourly_data         = SurveyForm.objects.filter(created_at__date = input_date).annotate(hour_of_day=ExtractHour('created_at')).values('hour_of_day').annotate(hourly_count=Count('id')).order_by('hour_of_day')
    return hourly_data
                

def get_attendance(input_date,ac_name):
    if ac_name != '':
        total_list = UserAttendance.objects.filter(punch_time__date = input_date,punch_status=0,
            user_id__in=User.objects.filter(ac_name=ac_name).values_list('id', flat=True)).values('user_id').distinct().count()
    else:
        total_list = UserAttendance.objects.filter(punch_time__date = input_date,punch_status='0').values('user_id').distinct().count()

    return total_list

class GetAgentPerformance(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access"}

        try:
            ac = request.data['ac']
            start_date = request.data['start_date']
            end_date = request.data['end_date']

            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            end_date = datetime.strptime(end_date, '%Y-%m-%d')


            total_questions = 20




            response_data = SurveyForm.objects.filter(
                ac_name = ac,
                created_at__range = (start_date, end_date + timedelta(days=1))
            ).values('id','surveyor__id','surveyor__fullname','qc_status')

            grouped_data = {}
            for response in response_data:
                surveyor_id = response['surveyor__id']
                if surveyor_id not in grouped_data:
                    grouped_data[surveyor_id] = {
                        'id' : response['id'],
                        'agent_id': response['surveyor__id'],
                        'agent_name': response['surveyor__fullname'],
                        'cf': 0,
                        'total_qc': 0,
                        'qla': 0,
                        'cla': 0,
                        'efficiency': 0,
                        'match_count' : 0,
                        'matched_count' : 0
                    }

                grouped_data[surveyor_id]['cf'] += 1

                if response['qc_status']:
                    grouped_data[surveyor_id]['total_qc'] += 1

                match_count_data = AgentQcResponseMatch.objects.filter(response_id=response['id']).values('match_count')
                match_count = 0

                if len(match_count_data) != 0 :
                    match_count = match_count_data[0]['match_count']

                grouped_data[surveyor_id]['match_count'] += match_count

                if match_count == total_questions:
                    grouped_data[surveyor_id]['matched_count'] += 1

                


            grouped_data_final  = []

            for surveyor_id,data in grouped_data.items():
                total_responses = data['total_qc']
                match_count_sum = data['match_count']
                matched_count = data['matched_count']
                days_difference = (end_date - start_date).days
                days_difference = days_difference+1
                qla = round((match_count_sum / (total_questions * total_responses)) * 100,2) if total_responses > 0 else 0
                cla = round((matched_count / total_responses) * 100,2) if total_responses > 0 else 0
                efficiency = round(100*(total_responses / (10 * days_difference)), 2) if days_difference > 0 else 0
                 

                x = {
                    "agent_id":surveyor_id,
                    "agent_name" : data['agent_name'],
                    "cf":data['cf'],
                    "total_qc" : data['total_qc'],
                    "qla" : qla,
                    "cla" : cla,
                    "efficiency": efficiency
                    
                }

                grouped_data_final.append(x)



            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = grouped_data_final

            
            return Response(result, status=status.HTTP_200_OK)
        
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class SurveyFormDetails(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            surveyor_id = request.data['agent_id']
            ac = request.data['ac']
            start_date = request.data['start_date']
            end_date = request.data['end_date']
            
            end_date=(datetime.strptime(end_date,'%Y-%m-%d') +timedelta(days=1)).strftime('%Y-%m-%d')
            response_data = SurveyForm.objects.filter(
                surveyor=surveyor_id,
                created_at__range=(start_date, end_date),
                ac_name=ac
            ).order_by('-created_at').values()


            for response in response_data:
                created_at_datetime = response['created_at']
                formatted_created_at = created_at_datetime.strftime("%Y-%m-%d %I:%M %p")
                response['created_at'] = formatted_created_at

                response['Response_id'] = response['id']
                response.pop('id', None) 
                response.pop('is_synced', None) 
                response.pop('updated_at', None) 
                response.pop('id_response', None) 
                response.pop('qc_status', None)
                response.pop('house_voters_count', None)
                response.pop('vote_decision_2019ae', None)
                response.pop('shd_current_mla_ticket_again', None)
                response.pop('sure_vote_current_mla', None)
                response.pop('education', None)
                response.pop('govt_hospital_rating', None)
                response.pop('education_facility_rating', None)
                response.pop('roads_rating', None)
                response.pop('electricity_rating', None)
                response.pop('drainage_rating', None)
                response.pop('member_voted_2019ae', None)
                response.pop('primary_consider_vote', None)
                response.pop('impt_election_issue_2024ae', None)
                response.pop('good_things_jagan_govt', None)
                response.pop('failure_jagan_govt', None)
                # response.pop('schemes_heard_jagan_govt', None)
                # response.pop('any_member_scheme_benefit', None)
                response.pop('shd_current_mla_ticket_again', None)
                response.pop('sure_vote_current_mla', None)
                response.pop('most_populous_caste', None)
                response.pop('other_most_populous_caste', None)
                response.pop('monthly_income_family', None)
                response.pop('things_possess', None)
                response.pop('house_type', None)
                response.pop('things_possess', None)

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = response_data

            return Response(result, status=status.HTTP_200_OK)
        
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class Get_Agent_forms(APIView):
    @handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}   
        try:
            user_data           =   request.user_data 
            if not user_data:
                result['result']['message']="Token expired. Please login again"
                return Response(result,status=status.HTTP_401_UNAUTHORIZED)
            
            agent_id = request.data['agent_id']
            agent_forms = SurveyForm.objects.filter(id = agent_id).values()
        
            total_count         =           agent_forms.count()
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Agent forms fetched successfully"
            result['result']['data']  = agent_forms
            result['result']['count'] = total_count
            return Response(result,status=status.HTTP_200_OK)         
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)

class QC_Performance(APIView):
    @handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}
        try:
            start_date = request.data["start_date"]
            end_date   = request.data["end_date"]
            ac_name         = request.data["ac"]
            if start_date == "" and end_date == "":
                start_date = "2024-02-15"
                end_date=(datetime.today()).strftime('%Y-%m-%d')
            end_date=(datetime.strptime(end_date,'%Y-%m-%d') +timedelta(days=1)).strftime('%Y-%m-%d')
            start_datetime = datetime.strptime(start_date, '%Y-%m-%d')
            end_datetime = datetime.strptime(end_date, '%Y-%m-%d')
        
            #days_difference = (end_datetime - start_datetime).days
            #target = 10
            user_info = User.objects.filter(role=3).values("id","fullname",'assigned_agent_id')
            user_ids = [i['id'] for i in user_info]
            qc_agent_mapping = []
            for i in user_info:
                if i['assigned_agent_id'] != '':
                    i['assigned_agent_id'] = list(map(int, i['assigned_agent_id'].split(',')))
                    qc_agent_mapping.append(i)
            #print("qc_agent_mapping",qc_agent_mapping) 
            data = QcSurveyForm.objects.filter(created_at__range=(start_date,end_date)).values()
            response_ids = [i['response_id_id'] for i in data]
            if ac_name != "":
                data = data.filter(ac_name=ac_name)

            data1 = SurveyForm.objects.filter(updated_at__range=("2024-02-01",end_date)).values()
            response_ids = [i['id'] for i in data1]
            if ac_name != "":
                data1 = data1.filter(ac_name=ac_name)
            total_target_val = []
            
            for i in qc_agent_mapping:
                target_val = data1.filter(surveyor__in=i['assigned_agent_id']).values('surveyor').annotate(total_target=Count('surveyor'))
                dict1 = {'id':i['id'],'qc_forms_assigned':0}
                for item in target_val:
                    dict1['qc_forms_assigned'] += item['total_target']
                    total_target_val.append(dict1)

            qced_data = data.filter(qc_agent_id__in=user_ids).values('qc_agent_id').annotate(total_qc=Count('qc_agent_id'))

            feed_back = SurveyFeedback.objects.filter(response_id__in=response_ids,feedback_by__in=user_ids).values('is_rel_qn','feedback_by')
            if ac_name != "":
                feed_back = feed_back.filter(response_id__ac_name=ac_name)
            feedback_formlevel = feed_back.filter(is_rel_qn=0).values('feedback_by').annotate(feedback_formlevel=Count('feedback_by'))
            feedback_que = feed_back.filter(is_rel_qn=1).values('feedback_by').annotate(feedback_questionlevel=Count('feedback_by'))
            final_response = {}
            
            for i in user_info:
                final_response[i['id']] = {'agent_id':i['id'],'agent_name':i['fullname'],'qc_forms_assigned':0,'total_qc':0,'feedback_formlevel':0,'feedback_questionlevel':0,'efficiency':0}
            
            for i in total_target_val:
                #print("i",i)
                #target_1 = 0
                if i['id'] in final_response.keys():
                    #target_1 = target_1+i['total_target']
                    final_response[i['id']]['qc_forms_assigned'] = i['qc_forms_assigned']

            for i in qced_data:
                if i['qc_agent_id'] in final_response.keys():
                    final_response[i['qc_agent_id']]['total_qc'] = i['total_qc']
            
            for i in feedback_formlevel:
                if int(i['feedback_by']) in final_response.keys():
                    final_response[int(i['feedback_by'])]['feedback_formlevel'] = i['feedback_formlevel']

            for i in feedback_que:
                if int(i['feedback_by']) in final_response.keys():
                    final_response[int(i['feedback_by'])]['feedback_questionlevel'] = i['feedback_questionlevel']

            final_response = list(final_response.values())

            final_response2 = []

            for response in final_response:
                if response['qc_forms_assigned'] == 0:
                    response['efficiency'] = 0.0
                else:
                    response['efficiency'] = round(100*(response['total_qc']/response['qc_forms_assigned']), 2)
                final_response2.append(response)

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "QC Data fetched successfully"
            result['result']['data']  = final_response2
            #result['result']['count'] = total_count
            return Response(result,status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)


class GetQuestions(APIView):
    @handle_auth_exceptions
    def get(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            data = [
                'area',
                'age',
                'gender',
                'occupation',
                'other_occupation',
                # 'education',
                # 'govt_hospital_rating',
                # 'education_facility_rating',
                # 'roads_rating',
                # 'electricity_rating',
                # 'drainage_rating',
                # 'member_voted_2019ae',
                'has_voted_2019ae',
                # 'vote_decision_2019ae',
                # 'primary_consider_vote',
                'party_voted_for_2019ae',
                'other_party_voted_for_2019ae',
                'party_voted_for_2019ge',
                'other_party_voted_for_2019ge',
                # 'impt_election_issue_2024ae',
                'party_vote_for_2024ae',
                'other_party_vote_for_2024ae',
                'reason_vote_2024ae',
                'pref_tdp_jsp_2024ae',
                'party_vote_for_2024ge',
                'other_party_vote_for_2024ge',
                'next_cm_2024ae',
                'other_next_cm_2024ae',
                'is_satisfied_jagan_govt',
                # 'good_things_jagan_govt',
                # 'failure_jagan_govt',
                'schemes_heard_jagan_govt',
                'any_member_scheme_benefit',
                'better_jagan_cbn',
                'shd_jagan_another_chance',
                'is_satisfies_mla',
                'party_can_win',
                'other_party_can_win',
                # 'most_populous_caste',
                # 'other_most_populous_caste',
                'caste',
                'other_caste',
                'social_category',
                'religion',
                'other_religion',
                # 'monthly_income_family',
                # 'things_possess',
                # 'house_type',
                'name',
                'phone_number',
                'second_party_vote_for_2024ae'
                ]
            
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data']  = data
            

            return Response(result,status=status.HTTP_200_OK)         
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
class SubmitFeedback(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            response_id = request.data['response_id']
            feedback_by = request.data['feedback_by']
            remarks     = request.data['remarks']
            is_rel_qn   = request.data['is_rel_qn']=='1'
            related_qn  = ""

            if is_rel_qn:
                related_qn = request.data['related_qn']


            surveyor_instance = get_object_or_404(SurveyForm, id=response_id) 

            created_at = datetime.today()

            SurveyFeedback.objects.create(
                response_id =  surveyor_instance,
                surveyor_id = surveyor_instance.surveyor,
                feedback_by = feedback_by,
                remarks =  remarks,
                is_rel_qn =  is_rel_qn,
                related_qn =  related_qn,
                created_at =  created_at
            )

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Feedback form created successfully"


            return Response(result,status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        

class GetFeedback(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            response_id = request.data['response_id']

            role_names = {
                1: 'Super Admin',
                3: 'QC Agent',
                4: 'Team Lead',
                5: 'Surveyor',
            }
            data = SurveyFeedback.objects.filter(response_id=response_id).values().order_by('-id')
            agent_ids = data.values_list('feedback_by', flat=True).distinct()
            user_queryset = User.objects.filter(id__in=agent_ids).values('fullname', 'role', 'id')
            user_map = {user_data['id']: user_data for user_data in user_queryset}

            result_list = []
            for entry in data:
                feedback_by_id = int(entry['feedback_by'])
                user_data = user_map.get(feedback_by_id, {})

                result_list.append({
                    'id': entry['id'],
                    'given_by_name': user_data.get('fullname', ''),
                    'given_by_role': role_names.get(user_data.get('role', ''), 'Unknown Role'),
                    'given_by_id': feedback_by_id,
                    'created_at': entry['created_at'].date(),
                    'response_id_id': entry['response_id_id'],
                    'surveyor_id_id': entry['surveyor_id_id'],
                    'remarks': entry['remarks'],
                    'is_rel_qn': entry['is_rel_qn'],
                    'related_qn': entry['related_qn'],
                })


            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = result_list

            return Response(result,status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Get_Details(APIView):
    @handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}   
        try:
            user_data           =   request.user_data 
            if not user_data:
                result['result']['message']="Token expired. Please login again"
                return Response(result,status=status.HTTP_401_UNAUTHORIZED)          
            response_id = request.data['response_id']
            data = QcSurveyForm.objects.filter(response_id = response_id).values()

            new_data = []
            for entry in data:
                new_entry = {}

                for key, value in entry.items():
                    if key.endswith("feedback"):
                        value_type = "audio" if isinstance(value, str) and value.startswith("http") else "text"
                        new_value = {
                            'value' : value,
                            'type'  : value_type
                        }
                        new_entry[key] = new_value
                    else:
                        new_entry[key] = value
                new_data.append(new_entry)
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data']  = new_data
            result['result']['count'] = len(new_data)

            return Response(result,status=status.HTTP_200_OK)         
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)

class Agent_By_QC_Performance(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            qc_agent_id = request.data['qc_agent_id']
            ac_name = request.data['ac']
            start_date = request.data['start_date']
            end_date = request.data['end_date']

            if start_date == "" and end_date == "":
                start_date = "2024-02-15"
                end_date=(datetime.today()).strftime('%Y-%m-%d')
            # 'id', 'response_id_id', 'surveyor_id_id', 'qc_agent_id_id', 'ac_name', 'created_at', 
                #    'recorded_audio',
            end_date=(datetime.strptime(end_date,'%Y-%m-%d') +timedelta(days=1)).strftime('%Y-%m-%d')     
            data = QcSurveyForm.objects.filter(created_at__range=(start_date,end_date),qc_agent_id=qc_agent_id).values()   
            

            #field_names = [field.name for field in AgentQcResponseMatch._meta.fields]
            #print(field_names)
            
            if ac_name != "":
                data = data.filter(ac_name=ac_name)
            qc_response_data = data.values('age','area', 'gender', 'occupation', 'education', 'govt_hospital_rating', 'education_facility_rating', 'roads_rating', 'electricity_rating', 'drainage_rating', 'has_voted_2019ae', 'party_voted_for_2019ae',
                    'party_voted_for_2019ge', 'party_vote_for_2024ae','second_party_vote_for_2024ae', 'pref_tdp_jsp_2024ae', 'party_vote_for_2024ge', 'next_cm_2024ae', 'is_satisfied_jagan_govt', 'better_jagan_cbn', 'shd_jagan_another_chance', 'is_satisfies_mla', 
                    'shd_current_mla_ticket_again', 'sure_vote_current_mla', 'party_can_win', 'caste', 'social_category', 'religion', 'monthly_income_family',
                    'response_id__age', 'response_id__area','response_id__gender', 'response_id__occupation', 'response_id__education', 'response_id__govt_hospital_rating', 
                    'response_id__education_facility_rating', 'response_id__roads_rating', 'response_id__electricity_rating', 'response_id__drainage_rating',
                    'response_id__has_voted_2019ae', 'response_id__party_voted_for_2019ae', 
                    'response_id__party_voted_for_2019ge', 'response_id__party_vote_for_2024ae', 'response_id__second_party_vote_for_2024ae', 'response_id__pref_tdp_jsp_2024ae', 'response_id__party_vote_for_2024ge',
                    'response_id__next_cm_2024ae', 'response_id__is_satisfied_jagan_govt', 'response_id__better_jagan_cbn', 'response_id__shd_jagan_another_chance', 'response_id__is_satisfies_mla', 'response_id__shd_current_mla_ticket_again',
                    'response_id__sure_vote_current_mla', 'response_id__party_can_win', 'response_id__caste', 'response_id__social_category', 'response_id__religion', 'response_id__monthly_income_family' )    
            
            qc_info = data.values('id', 'response_id__surveyor', 'qc_agent_id_id', 'ac_name', 'created_at','recorded_audio')
            
            data2 = data.values_list('response_id',flat=True)

            #remove_fields = ['response_id_id' 'surveyor_id_id','created_at']
            #selected_fields = ['s1q1_client_gender_match', 's1q4_client_occupation_match', 's1q5_client_education_match', 's2q6_availed_govtscheme_match', 's2q8_is_difficulties_scheme_match', 's2q10_is_govtschools_near_match', 's2q11_is_improved_govtschools_match', 's2q14_is_govthospitals_near_match', 's2q15_is_improved_govthospitals_match', 's3q19_client_is_voted_match', 's3q21_choose_to_ppv_match', 's3q22_prev_voted_party_match', 's3q23_female_vote_decision_match', 's3q19_choosed_party_tovote_match', 's3q27_reason_diff_choosedparty_match', 's3q29_alliance_choose_party_match', 's3q30_attention_to_vote_match', 's3q31_satisfied_with_currgovt_match', 's3q32_satisfied_with_currmla_match', 's3q33_next_cm_opinion_match', 's3q34_choose_ac_candidate_match', 's3q35_next_mla_opinion_match', 's4q37_client_caste_match', 's4q38_client_social_category_match', 's4q39_client_religion_match', 's5q40_is_comfort_personalinfo_match', 's5q43_client_monthly_income_match', 's5q44_house_type_livein_match', 's5q45_client_having_things_match', 'match_count']
            match_que = AgentQcResponseMatch.objects.filter(response_id__in=data2).values()
            #match_que = match_data.values(*selected_fields)
            question_list=['age','area', 'gender', 'occupation', 'has_voted_2019ae', 'party_voted_for_2019ae', 'party_voted_for_2019ge', 'party_vote_for_2024ae', 'second_party_vote_for_2024ae', 'pref_tdp_jsp_2024ae', 'party_vote_for_2024ge', 'next_cm_2024ae', 'is_satisfied_jagan_govt', 'better_jagan_cbn', 'shd_jagan_another_chance', 'is_satisfies_mla', 'party_can_win', 'caste', 'social_category', 'religion']
            question = []
            
            total_questions = 35
            obj_questions = 20
            index=0
            for response in qc_response_data:
                res = {'qc_id':0,'agent_id':0,'date':0,'recording':0,'response_analysis':0}
                for k in response:
                    k2 = "response_id__"+k
                    if not k.startswith('response_id__') and k in question_list:
                        res[k] = [{
                                    "agent_response": 0,
                                    "qc_response": response[k],
                                    "match": False
                                }]
                    if k in res.keys():
                      res[k][0]['agent_response']=response[k2]

                question.append(res)
    
            for val in range(len(question)):
                question[val]['qc_id'] = qc_info[val]['qc_agent_id_id']
                question[val]['agent_id'] = qc_info[val]['response_id__surveyor']
                question[val]['date'] = qc_info[val]['created_at'].date()
                question[val]['recording'] = qc_info[val]['recorded_audio']
                question[val]['response_analysis'] = [ {
                                 "Total_Questions": total_questions,
                                 "Objective_Questions": obj_questions,
                                 "Matched_Questions": match_que[val]['match_count'],
                                 "Unmatched_Questions": obj_questions - int(match_que[val]['match_count'])
                             }]
                for i in question_list:
                    new_key = i + '_match'
                    question[val][i][0]['match'] = match_que[val][new_key]
        
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = question

            return Response(result, status=status.HTTP_200_OK)
        
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetQCListing(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:

            search_string = request.data['search_string']
            filter_object = Q(role=3)
            if search_string != '':
                search_condition = (
                    Q(id__icontains=search_string) |
                    Q(fullname__icontains=search_string) |
                    Q(email__icontains=search_string)
                )
                filter_object &= search_condition


            response_data = User.objects.filter(filter_object).values()

            if len(response_data) == 0:
                result['status'] = "OK"
                result['valid'] = True
                result['result']['message'] = "Data fetched successfully"
                result['result']['data'] = []


            final_response = []

            for response in response_data:
                agent_ids = response['assigned_agent_id']


                if agent_ids is not None and agent_ids:
                    agent_ids = [int(agent_id) for agent_id in agent_ids.split(',')]
                    
                else:
                    agent_ids  = []

                agent_data = []

                for agent_id in agent_ids:

                    agent_name = User.objects.filter(id=agent_id).values('fullname')

                    

                    x = {
                        "agent_id" : agent_id,
                        "agent_name" : agent_name[0]['fullname']
                    }

                    agent_data.append(x)


                
                response.pop('password', None)
                response.pop('assigned_agent_id', None)
                response['assigned_agent_data'] = agent_data

                final_response.append(response)
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = final_response

            return Response(result, status=status.HTTP_200_OK)
            
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class UpdateQCListing(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            qc_agent_id = request.data['qc_agent_id']
            assigned_agent_id = request.data['assigned_agent_id']

            agent_data_list = User.objects.filter(id = qc_agent_id)

            if len(agent_data_list) == 0:
                result['result']['message'] = "No QC Agent with this id exists"
                return Response(result,status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            
            agent_data = agent_data_list.first()

            agent_data.assigned_agent_id = assigned_agent_id
            agent_data.save()

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data updated successfully"

            return Response(result, status=status.HTTP_200_OK)

        except Exception as e:
            result['result']['message'] = str(e)
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class SurveySubmit(APIView):
    # @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access"}

        try :
            SurveyForm.objects.create(
                surveyor_id             =   request.data['surveyor'],
                created_at              =   datetime.today(),
                recorded_audio          =   request.data['recorded_audio'],
                district_name           =   request.data['district_name'],
                ac_name                 =   request.data['ac_name'],
                booth_number            =   request.data['booth_number'],
                area                    =   request.data['area'],
                age                         =request.data['age'],
                gender                      =request.data['gender'],
                occupation                  =request.data['occupation'],
                education                   =request.data['education'],
                govt_hospital_rating        =request.data['govt_hospital_rating'],
                education_facility_rating   =request.data['education_facility_rating'],
                roads_rating                =request.data['roads_rating'],
                electricity_rating          =request.data['electricity_rating'],
                drainage_rating             =request.data['drainage_rating'],
                house_voters_count          =request.data['house_voters_count'],
                member_voted_2019ae         =request.data['member_voted_2019ae'],
                has_voted_2019ae            =request.data['has_voted_2019ae'],
                primary_consider_vote       =request.data['primary_consider_vote'],
                vote_decision_2019ae        =request.data['vote_decision_2019ae'],
                party_voted_for_2019ae      =request.data['party_voted_for_2019ae'],
                other_party_voted_for_2019ae=request.data['other_party_voted_for_2019ae'],
                party_voted_for_2019ge      =request.data['party_voted_for_2019ge'],
                other_party_voted_for_2019ge=request.data['other_party_voted_for_2019ge'],
                impt_election_issue_2024ae  =request.data['impt_election_issue_2024ae'],
                party_vote_for_2024ae       =request.data['party_vote_for_2024ae'],
                other_party_vote_for_2024ae = request.data['other_party_vote_for_2024ae'],
                reason_vote_2024ae          =request.data['reason_vote_2024ae'],
                pref_tdp_jsp_2024ae         =request.data['pref_tdp_jsp_2024ae'],
                party_vote_for_2024ge       =request.data['party_vote_for_2024ge'],
                other_party_vote_for_2024ge = request.data['other_party_vote_for_2024ge'],
                next_cm_2024ae              =request.data['next_cm_2024ae'],
                other_next_cm_2024ae        =request.data['other_next_cm_2024ae'],
                is_satisfied_jagan_govt     =request.data['is_satisfied_jagan_govt'],
                good_things_jagan_govt      =request.data['good_things_jagan_govt'],
                failure_jagan_govt          =request.data['failure_jagan_govt'],
                schemes_heard_jagan_govt    =request.data['schemes_heard_jagan_govt'],
                any_member_scheme_benefit   =request.data['any_member_scheme_benefit'],
                better_jagan_cbn            =request.data['better_jagan_cbn'],
                shd_jagan_another_chance    =request.data['shd_jagan_another_chance'],
                is_satisfies_mla            =request.data['is_satisfies_mla'],
                shd_current_mla_ticket_again=request.data['shd_current_mla_ticket_again'],
                sure_vote_current_mla       =request.data['sure_vote_current_mla'],
                party_can_win               =request.data['party_can_win'],
                other_party_can_win         =request.data['other_party_can_win'],
                most_populous_caste         =request.data['most_populous_caste'],
                other_most_populous_caste   =request.data['other_most_populous_caste'],
                caste                       =request.data['caste'],
                other_caste                 = request.data['other_caste'],
                social_category             = request.data['social_category'],
                religion                    =request.data['religion'],
                other_religion              = request.data['other_religion'],
                monthly_income_family       =request.data['monthly_income_family'],
                things_possess              =request.data['things_possess'],
                house_type                  =request.data['house_type'],
                name                        =request.data['name'],
                phone_number                =request.data['phone_number'],
                geo_address                 = request.data['geo_address'],
                longitude                   = request.data['longitude'],
                latitude                    = request.data['latitude'],
                ip_address                  = get_client_ip(request),
                is_synced                     = 1
            )
            if 'electoral_id' in request.data and request.data['electoral_id'] != '':
                electoraldata = ElectoralList.objects.get(id=request.data['electoral_id'])
                electoraldata.survey_status = 1
                electoraldata.surveyed_timestamp = datetime.today()
                electoraldata.surveyed_by_id = request.data['surveyor']
                if 'is_found' in request.data and request.data['is_found'] != '':
                    electoraldata.is_found = request.data['is_found']
                electoraldata.save()


            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Response QC successfully"
            return Response(result,status=status.HTTP_200_OK)

        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class Get_AgentListing(APIView):
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')
            ac_name    = request.data.get('ac_name')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            #filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            today = datetime.today().date()
            users_info   = User.objects.filter(role=5).values('id','fullname','ac_name','mapped_booths')
            if ac_name != '':
                users_info = users_info.filter(ac_name=ac_name).values('id','fullname','ac_name','mapped_booths')
            userQueryset = users_info.values_list('id', flat=True)
            attQueryset  = UserAttendance.objects.filter(
                    user_id__in =list(userQueryset), 
                    punch_time__date = datetime.today(), 
                    punch_status = 0).values('user_id').distinct()

            details = users_info.filter(id__in=attQueryset).values('id','fullname','ac_name','mapped_booths')
            data                = SurveyForm.objects.filter(created_at__range=(start_date,end_date_new),surveyor__in=attQueryset).values()
            if ac_name != '':
                data = data.filter(ac_name=ac_name)
            data1 = data.filter(created_at__range=(end_date,end_date_new)).values('surveyor').annotate(form_today=Count('id'))
            data2 = data.values('surveyor').annotate(form_cumulative=Count('surveyor'))
            final_response = {}
            for i in details:
                final_response[i['id']] = {'agent_id':i['id'],'agent_name':i['fullname'],'ac':i['ac_name'],'booth':i['mapped_booths'],'form_cumulative':0,'form_today':0}
            
            for i in data2:
                if i['surveyor'] in final_response.keys():
                    final_response[i['surveyor']]['form_cumulative'] = i['form_cumulative']
            
            for i in data1:
                if i['surveyor'] in final_response.keys():
                    final_response[i['surveyor']]['form_today'] = i['form_today']
            final_response = list(final_response.values())
            result['status']    = 'OK'
            result['valid']     =  True
            result['result']['message'] = "Data fetched successfully."
            result['result']['data']    = final_response

            return Response(result,status = status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Get_Agentforms(APIView):
    #@handle_auth_exceptions
    def post(self,request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}   
        try:
            agent_id = request.data['agent_id']
            flag     = request.data['flag']
            ac_name  = request.data['ac_name']
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            #start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            
            today = datetime.today().date()
            data  = SurveyForm.objects.filter(surveyor = agent_id).values()
            if ac_name != '':
                data = data.filter(ac_name=ac_name)
            if flag == '0':
                data = data.filter(created_at__range=(start_date,end_date_new))
            if flag == '1':
                data = data.filter(created_at__range=(end_date,end_date_new))
            
            for entry in data:
                ordered_entry = {
                    'surveyor_id'   : entry.get('surveyor_id'),
                    'response_id'   : entry.get('id'),
                    'name'          : entry.get('name'),  
                    'phone_number'  : entry.get('phone_number'),  
                }
                
                # entry['qc_status']   = "1" if entry.get('qc_status') else "0"
                entry['created_at']  = entry['created_at'].date()
                entry['updated_at']  = entry['updated_at'].date()

                pop_list = ['id', 'name', 'phone_number', 'surveyor_id', 'govt_hospital_rating', 
                            'education_facility_rating', 'roads_rating', 'electricity_rating', 'drainage_rating', 'house_voters_count', 
                            'member_voted_2019ae', 'primary_consider_vote', 'vote_decision_2019ae', 'impt_election_issue_2024ae', 'good_things_jagan_govt', 
                            'failure_jagan_govt', 'shd_current_mla_ticket_again','sure_vote_current_mla', 'most_populous_caste', 'monthly_income_family', 
                            'things_possess', 'house_type', 'id_response', 'other_most_populous_caste']
                
                for key in pop_list:
                    entry.pop(key, None)
                
                ordered_entry.update(entry)
                entry.clear()
                entry.update(ordered_entry)

            # if ac_name != '':
            #     data = data.filter(ac_name=ac_name)
            # if flag == '0':
            #     data = data.filter(created_at__range=(start_date,end_date_new))
            # if flag == '1':
            #     data = data.filter(created_at__range=(end_date,end_date_new))
            #agent_forms = SurveyForm.objects.filter(surveyor = agent_id).values()
        
            total_count         =           data.count()
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Agent forms fetched successfully"
            result['result']['data']  = data
            result['result']['count'] = total_count
            return Response(result,status=status.HTTP_200_OK)         
        except Exception as e:
            result['result']['message']=str(e)
            return Response(result,status=status.HTTP_400_BAD_REQUEST)

class DetailedFeedback(APIView) :
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            inp_id = int(request.data['response_id'])

            # match_obj_queryset = AgentQcResponseMatch.objects.filter(response_id = inp_id).values()

            queryset = SurveyFormQuestionnaire.objects.values()
            question_list = queryset.values_list('questionnaire_field', flat = True)
            question_queryset = queryset.values('questionnaire_field', 'questionnaire_value')
            mapping = {item['questionnaire_field']: item['questionnaire_value'] for item in question_queryset}

            formatted_question_list = [field + '_feedback' for field in question_list]

            qc_formatted_response = QcSurveyForm.objects.filter(response_id = inp_id).values(*(formatted_question_list))
            qc_formatted_response = [{k: v for k, v in item.items() if v is not None and v != ''} for item in qc_formatted_response]

            sr_response = SurveyForm.objects.filter(id = inp_id).values()
            qc_response = QcSurveyForm.objects.filter(response_id = inp_id).values()
            
            result_list = []
            for entry in qc_formatted_response:
                for question, feedback in entry.items():
                    question = question[:-9] 
                    surveyor_response = next((item[question] for item in sr_response if question in item), None)
                    agent_response = next((item[question] for item in qc_response if question in item), None)

                    result_list.append({
                        'question': mapping.get(question, 0),
                        'agent_response' : surveyor_response,
                        'qc_response' : agent_response,
                        'qc_feedback': feedback,
                        'type': "audio" if isinstance(feedback, str) and feedback.startswith("http") else "text"
                    })

            result['status']="OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['data'] = result_list
            return Response(result,status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        