from django.apps import AppConfig


class AgentResponseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'agent_response'
