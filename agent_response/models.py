from django.db import models
from django.utils import timezone
from users.models import *
# Create your models here.

# class User(models.Model):
#     fullname                        = models.CharField(max_length=150, blank=True, null=True)
#     phone_number                    = models.CharField(max_length=150, blank=True, null=True,unique=True)
#     email                           = models.CharField(max_length=255, blank=True, null=True)
#     password                        = models.CharField(max_length=1024, blank=True, null=True)
#     emp_code                        = models.CharField(max_length=200, blank=True, null=True)
#     role                            = models.IntegerField(blank=True, null=True, default=0)
#     ac_name                         = models.CharField(max_length=200, blank=True, null=True)
#     otp                             = models.IntegerField(blank=True, null=True, default=0)
#     opt_datetime                    = models.DateTimeField(blank=True, null=True)
#     status                          = models.IntegerField(blank=True, null=True, default=1)
#     registered_on                   = models.DateTimeField(default=timezone.now,blank=True, null=True)
#     assigned_agent_id               = models.CharField(max_length=1024, blank=True, null=True)
#     district                        = models.CharField(max_length=200, blank=True, null=True)
#     mapped_booths                   = models.CharField(max_length=255, blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'tbl_users'

class SurveyForm(models.Model):
    #general surveyor info
    surveyor                                    = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at                                  = models.DateTimeField(blank=True, null=True)
    is_synced                                   = models.IntegerField(blank=True, null=True, default=0)
    updated_at                                  = models.DateTimeField(auto_now_add=True,blank=True, null=True)
    recorded_audio                              = models.TextField(blank=True, null=True)
    
    district_name                               = models.CharField(max_length=100,blank=True,null=True)
    area                                        = models.CharField(max_length=100,blank=True,null=True)    
    ac_name                                     = models.CharField(max_length=100, blank=True, null=True)
    booth_number                                = models.CharField(max_length = 50, blank=True, null=True)
    age                                         = models.IntegerField(blank=True, null=True, default=0)
    gender                                      = models.CharField(max_length=50, blank=True, null=True)
    occupation                                  = models.TextField(blank=True, null=True)
    other_occupation                            = models.TextField(null=True,blank=True)
    education                                   = models.CharField(max_length=255, blank=True, null=True)
    
    govt_hospital_rating                        = models.CharField(max_length=50,blank=True, null=True)
    education_facility_rating                   = models.CharField(max_length=50,blank=True, null=True)
    roads_rating                                = models.CharField(max_length=50,blank=True, null=True)
    electricity_rating                          = models.CharField(max_length=50,blank=True, null=True)
    drainage_rating                             = models.CharField(max_length=50,blank=True, null=True)

    house_voters_count                          = models.CharField(max_length=20,blank=True, null=True)
    member_voted_2019ae                         = models.CharField(max_length=20,blank=True, null=True)
    has_voted_2019ae                            = models.CharField(max_length=20,null=True,blank=True)
    primary_consider_vote                       = models.TextField(null=True,blank=True)
    vote_decision_2019ae                        = models.TextField(null=True,blank=True)
    party_voted_for_2019ae                      = models.CharField(max_length=255,null=True,blank=True)
    other_party_voted_for_2019ae                = models.TextField(null=True,blank=True)
    party_voted_for_2019ge                      = models.CharField(max_length=255,null=True,blank=True)
    other_party_voted_for_2019ge                = models.TextField(null=True,blank=True)
    impt_election_issue_2024ae                  = models.TextField(null=True,blank=True)
    party_vote_for_2024ae                       = models.CharField(max_length=255,null=True,blank=True)
    other_party_vote_for_2024ae                 = models.TextField(null=True,blank=True)
    second_party_vote_for_2024ae                = models.CharField(max_length=255,null=True,blank=True)
    other_second_party_vote_for_2024ae          = models.TextField(null=True,blank=True)
    reason_vote_2024ae                          = models.TextField(null=True,blank=True)
    pref_tdp_jsp_2024ae                         = models.CharField(max_length=255,null=True,blank=True)
    other_pref_tdp_jsp_2024ae                   = models.TextField(null=True,blank=True)
    party_vote_for_2024ge                       = models.CharField(max_length=255,null=True,blank=True)
    other_party_vote_for_2024ge                 = models.TextField(null=True,blank=True)
    next_cm_2024ae                              = models.CharField(max_length=255,null=True,blank=True)
    other_next_cm_2024ae                        = models.TextField(null=True,blank=True)               
    is_satisfied_jagan_govt                     = models.CharField(max_length=50,null=True,blank=True)
    good_things_jagan_govt                      = models.TextField(null=True,blank=True)
    failure_jagan_govt                          = models.TextField(null=True,blank=True)
    schemes_heard_jagan_govt                    = models.TextField(null=True,blank=True)
    other_schemes_heard_jagan_govt              = models.TextField(null=True,blank=True)
    any_member_scheme_benefit                   = models.CharField(max_length=50,null=True,blank=True)
    better_jagan_cbn                            = models.CharField(max_length=50,null=True,blank=True)
    shd_jagan_another_chance                    = models.CharField(max_length=50,null=True,blank=True)
    is_satisfies_mla                            = models.CharField(max_length=50,null=True,blank=True)
    shd_current_mla_ticket_again                = models.CharField(max_length=50,null=True,blank=True)
    sure_vote_current_mla                       = models.CharField(max_length=50,null=True,blank=True)
    party_can_win                               = models.CharField(max_length=255,null=True,blank=True)
    other_party_can_win                         = models.TextField(null=True,blank=True)
    most_populous_caste                         = models.TextField(null=True,blank=True)
    other_most_populous_caste                   = models.TextField(null=True,blank=True)
    caste                                       = models.TextField(null=True,blank=True)
    other_caste                                 = models.TextField(null=True,blank=True)
    social_category                             = models.CharField(max_length=255,null=True,blank=True)
    religion                                    = models.CharField(max_length=255,null=True,blank=True)
    other_religion                              =models.TextField(null=True,blank=True)
    monthly_income_family                       = models.CharField(max_length=255,null=True,blank=True)
    things_possess                              = models.TextField(null=True,blank=True)
    house_type                                  = models.TextField(null=True,blank=True)
    name                                        = models.CharField(max_length=255,null=True,blank=True)
    phone_number                                = models.CharField(max_length=20,null=True,blank=True)
    ip_address                                  = models.CharField(max_length=255,null=True,blank=True)
    geo_address                                 = models.TextField(null = True,blank = True)
    longitude                                   = models.CharField(max_length=255,null=True,blank=True)
    latitude                                    = models.CharField(max_length=255,null=True,blank=True)
    id_response                                 = models.IntegerField(blank = True, null = True, default = 0)
    qc_status                                   = models.BooleanField(default=False)
    electoral                                   = models.ForeignKey(ElectoralList,on_delete=models.CASCADE,default=None,null=True)


    class Meta:
        managed = True
        db_table = 'tbl_survey'

class QcSurveyForm(models.Model):
    response_id                              = models.ForeignKey(SurveyForm,on_delete=models.CASCADE,unique=True)
    surveyor_id                              = models.ForeignKey(User,on_delete=models.CASCADE,related_name='surveyor_forms')
    qc_agent_id                              = models.ForeignKey(User,on_delete=models.CASCADE,related_name='qc_agent_forms',null=True, default=None)
    ac_name                                  = models.CharField(max_length=100, blank=True, null=True)
    created_at                               = models.DateTimeField(blank=True, null=True)
    recorded_audio                           = models.TextField(blank=True, null=True)

    area                                        = models.CharField(max_length=100,blank=True,null=True)
    age                                         = models.IntegerField(blank=True, null=True, default=0)
    gender                                      = models.CharField(max_length=50, blank=True, null=True)
    occupation                                  = models.TextField(blank=True, null=True)
    other_occupation                            = models.TextField(null=True,blank=True)
    education                                   = models.CharField(max_length=255, blank=True, null=True) 
    govt_hospital_rating                        = models.CharField(max_length=50,blank=True, null=True)
    education_facility_rating                   = models.CharField(max_length=50,blank=True, null=True)
    roads_rating                                = models.CharField(max_length=50,blank=True, null=True)
    electricity_rating                          = models.CharField(max_length=50,blank=True, null=True)
    drainage_rating                             = models.CharField(max_length=50,blank=True, null=True)
    house_voters_count                          = models.CharField(max_length=20,blank=True, null=True)
    member_voted_2019ae                         = models.CharField(max_length=20,blank=True, null=True)
    has_voted_2019ae                            = models.CharField(max_length=20,null=True,blank=True)
    primary_consider_vote                       = models.TextField(null=True,blank=True)
    vote_decision_2019ae                        = models.TextField(null=True,blank=True)
    party_voted_for_2019ae                      = models.CharField(max_length=255,null=True,blank=True)
    other_party_voted_for_2019ae                = models.TextField(null=True,blank=True)
    party_voted_for_2019ge                      = models.CharField(max_length=255,null=True,blank=True)
    other_party_voted_for_2019ge                = models.TextField(null=True,blank=True)
    impt_election_issue_2024ae                  = models.TextField(null=True,blank=True)
    party_vote_for_2024ae                       = models.CharField(max_length=255,null=True,blank=True)
    other_party_vote_for_2024ae                 = models.TextField(null=True,blank=True)
    second_party_vote_for_2024ae                = models.CharField(max_length=255,null=True,blank=True)
    other_second_party_vote_for_2024ae          = models.TextField(null=True,blank=True)
    reason_vote_2024ae                          = models.TextField(null=True,blank=True)
    pref_tdp_jsp_2024ae                         = models.CharField(max_length=255,null=True,blank=True)
    other_pref_tdp_jsp_2024ae                   = models.TextField(null=True,blank=True)
    party_vote_for_2024ge                       = models.CharField(max_length=255,null=True,blank=True)
    other_party_vote_for_2024ge                 = models.TextField(null=True,blank=True)
    next_cm_2024ae                              = models.CharField(max_length=255,null=True,blank=True)
    other_next_cm_2024ae                        = models.TextField(null=True,blank=True)               
    is_satisfied_jagan_govt                     = models.CharField(max_length=50,null=True,blank=True)
    good_things_jagan_govt                      = models.TextField(null=True,blank=True)
    failure_jagan_govt                          = models.TextField(null=True,blank=True)
    schemes_heard_jagan_govt                    = models.TextField(null=True,blank=True)
    other_schemes_heard_jagan_govt              = models.TextField(null=True,blank=True)
    any_member_scheme_benefit                   = models.CharField(max_length=50,null=True,blank=True)
    better_jagan_cbn                            = models.CharField(max_length=50,null=True,blank=True)
    shd_jagan_another_chance                    = models.CharField(max_length=50,null=True,blank=True)
    is_satisfies_mla                            = models.CharField(max_length=50,null=True,blank=True)
    shd_current_mla_ticket_again                = models.CharField(max_length=50,null=True,blank=True)
    sure_vote_current_mla                       = models.CharField(max_length=50,null=True,blank=True)
    party_can_win                               = models.CharField(max_length=255,null=True,blank=True)
    other_party_can_win                         = models.TextField(null=True,blank=True)
    most_populous_caste                         = models.TextField(null=True,blank=True)
    other_most_populous_caste                   = models.TextField(null=True,blank=True)
    caste                                       = models.TextField(null=True,blank=True)
    other_caste                                 = models.TextField(null=True,blank=True)
    social_category                             = models.CharField(max_length=255,null=True,blank=True)
    religion                                    = models.CharField(max_length=255,null=True,blank=True)
    other_religion                              =models.TextField(null=True,blank=True)
    monthly_income_family                       = models.CharField(max_length=255,null=True,blank=True)
    things_possess                              = models.TextField(null=True,blank=True)
    house_type                                  = models.TextField(null=True,blank=True)
    name                                        = models.CharField(max_length=255,null=True,blank=True)
    phone_number                                = models.CharField(max_length=20,null=True,blank=True)
    
    # Feedback
    age_feedback                                = models.TextField(null=True,blank=True)
    gender_feedback                             = models.TextField(null=True,blank=True)
    area_feedback                               = models.TextField(null=True,blank=True)
    occupation_feedback                         = models.TextField(null=True,blank=True)
    #other_occupation_feedback                   = models.TextField(null=True, blank=True)
    education_feedback                          = models.TextField(null=True,blank=True)
    govt_hospital_rating_feedback               = models.TextField(null=True,blank=True)
    education_facility_rating_feedback          = models.TextField(null=True,blank=True)
    roads_rating_feedback                       = models.TextField(null=True,blank=True)
    electricity_rating_feedback                 = models.TextField(null=True,blank=True)
    drainage_rating_feedback                    = models.TextField(null=True,blank=True)
    house_voters_count_feedback                 = models.TextField(null=True,blank=True)
    member_voted_2019ae_feedback                = models.TextField(null=True,blank=True)
    has_voted_2019ae_feedback                   = models.TextField(null=True,blank=True)
    primary_consider_vote_feedback              = models.TextField(null=True,blank=True)
    vote_decision_2019ae_feedback               = models.TextField(null=True,blank=True)
    party_voted_for_2019ae_feedback             = models.TextField(null=True,blank=True)
    #other_party_voted_for_2019ae_feedback       = models.TextField(null=True,blank=True)
    party_voted_for_2019ge_feedback             = models.TextField(null=True,blank=True)
    #other_party_voted_for_2019ge_feedback       = models.TextField(null=True,blank=True)
    impt_election_issue_2024ae_feedback         = models.TextField(null=True,blank=True)
    party_vote_for_2024ae_feedback              = models.TextField(null=True,blank=True)
    second_party_vote_for_2024ae_feedback       = models.TextField(null=True,blank=True)
    #other_party_vote_for_2024ae_feedback        = models.TextField(null=True,blank=True)
    reason_vote_2024ae_feedback                 = models.TextField(null=True,blank=True)
    pref_tdp_jsp_2024ae_feedback                = models.TextField(null=True,blank=True)
    party_vote_for_2024ge_feedback              = models.TextField(null=True,blank=True)
    #other_party_vote_for_2024ge_feedback        = models.TextField(null=True,blank=True)
    next_cm_2024ae_feedback                     = models.TextField(null=True,blank=True)
    #other_next_cm_2024ae_feedback               = models.TextField(null=True,blank=True)
    is_satisfied_jagan_govt_feedback            = models.TextField(null=True,blank=True)
    good_things_jagan_govt_feedback             = models.TextField(null=True,blank=True)
    failure_jagan_govt_feedback                 = models.TextField(null=True,blank=True)
    schemes_heard_jagan_govt_feedback           = models.TextField(null=True,blank=True)
    any_member_scheme_benefit_feedback          = models.TextField(null=True,blank=True)
    better_jagan_cbn_feedback                   = models.TextField(null=True,blank=True)
    shd_jagan_another_chance_feedback           = models.TextField(null=True,blank=True)
    is_satisfies_mla_feedback                   = models.TextField(null=True,blank=True)
    shd_current_mla_ticket_again_feedback       = models.TextField(null=True,blank=True)
    sure_vote_current_mla_feedback              = models.TextField(null=True,blank=True)
    party_can_win_feedback                      = models.TextField(null=True,blank=True)
    #other_party_can_win_feedback                = models.TextField(null=True,blank=True)
    most_populous_caste_feedback                = models.TextField(null=True,blank=True)
    #other_most_populous_caste_feedback          = models.TextField(null=True,blank=True)
    caste_feedback                              = models.TextField(null=True,blank=True)
    #other_caste_feedback                        = models.TextField(null=True,blank=True)
    social_category_feedback                    = models.TextField(null=True,blank=True)
    religion_feedback                           = models.TextField(null=True,blank=True)
    #other_religion_feedback                     = models.TextField(null=True,blank=True)
    monthly_income_family_feedback              = models.TextField(null=True,blank=True)
    things_possess_feedback                     = models.TextField(null=True,blank=True)
    house_type_feedback                         = models.TextField(null=True,blank=True)
    name_feedback                               = models.TextField(null=True,blank=True)
    phone_number_feedback                       = models.TextField(null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'tbl_qc_response'


class AgentQcResponseMatch(models.Model):
    #general surveyor info
    surveyor_id                              = models.ForeignKey(User,on_delete = models.CASCADE)
    response_id                              = models.ForeignKey(SurveyForm,on_delete=models.CASCADE,unique=True)
    created_at                               = models.DateTimeField(blank=True, null=True)

    age_match                                 =  models.BooleanField(default=False) 
    area_match                                =  models.BooleanField(default=False) 
    gender_match                              =  models.BooleanField(default=False) 
    occupation_match                          =  models.BooleanField(default=False)
    # education_match                           =  models.BooleanField(default=False)
    # govt_hospital_rating_match                =  models.BooleanField(default=False)
    # education_facility_rating_match           =  models.BooleanField(default=False)
    # roads_rating_match                        =  models.BooleanField(default=False)
    # electricity_rating_match                  =  models.BooleanField(default=False)
    # drainage_rating_match                     =  models.BooleanField(default=False)

    has_voted_2019ae_match                    = models.BooleanField(default=False)
    party_voted_for_2019ae_match              = models.BooleanField(default=False)
    party_voted_for_2019ge_match              = models.BooleanField(default=False)
    party_vote_for_2024ae_match               = models.BooleanField(default=False)
    second_party_vote_for_2024ae_match        = models.BooleanField(default=False)
    pref_tdp_jsp_2024ae_match                 = models.BooleanField(default=False)
    party_vote_for_2024ge_match               = models.BooleanField(default=False)
    next_cm_2024ae_match                      = models.BooleanField(default=False)
    is_satisfied_jagan_govt_match             = models.BooleanField(default=False)
    better_jagan_cbn_match                    = models.BooleanField(default=False)
    shd_jagan_another_chance_match            = models.BooleanField(default=False)
    is_satisfies_mla_match                    = models.BooleanField(default=False)
    # shd_current_mla_ticket_again_match        = models.BooleanField(default=False)
    # sure_vote_current_mla_match               = models.BooleanField(default=False)
    party_can_win_match                       = models.BooleanField(default=False)
    caste_match                               = models.BooleanField(default=False)
    social_category_match                     = models.BooleanField(default=False)
    religion_match                            = models.BooleanField(default=False)
    # monthly_income_family_match               = models.BooleanField(default=False)
    match_count                               = models.IntegerField(blank=True, null=True, default=0)


    class Meta:
        managed = True
        db_table = 'tbl_agent_qc_response_match'

class Raiserequests(models.Model):
    phone_number                    = models.CharField(max_length=150, blank=True, null=True)
    email                           = models.CharField(max_length=255, blank=True, null=True)
    emp_code                        = models.CharField(max_length=200, blank=True, null=True)
    role                            = models.IntegerField(blank=True, null=True, default=0)
    requested_on                    = models.DateTimeField(blank=True, null=True)
    is_verified                     = models.IntegerField(blank=True, null=True, default=0)
    verified_at                     = models.DateTimeField(blank=True, null=True)
    admin_verified                  = models.IntegerField(blank=True, null=True, default=0)
    admin_verfied_at                = models.DateTimeField(blank=True, null=True)
    email_pin                       = models.IntegerField(blank=True, null=True, default=0)
    pin_datetime                    = models.DateTimeField(blank=True, null=True)

    
    class Meta:
        managed = False
        db_table = 'tbl_raiserequest'

class Banner(models.Model):
    media_url                       = models.CharField(max_length=255, blank=True, null=True)
    media_thumb                     = models.CharField(max_length=255, blank=True, null=True)
    media_type                      = models.CharField(max_length=50, blank=True, null=True)
    notif_type                      = models.CharField(max_length=50, blank=True, null=True,verbose_name="generic,role,user",)
    notify_to                       = models.CharField(max_length=255, blank=True, null=True,verbose_name="Place role or userid with commaseperated and only userspecific is comma seperated.")
    created_at                      = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_banners'

class SurveyFeedback(models.Model):
    response_id                              = models.ForeignKey(SurveyForm,on_delete=models.CASCADE)
    surveyor_id                              = models.ForeignKey(User,on_delete=models.CASCADE)
    feedback_by                              = models.CharField(max_length=100, blank=True, null=True)
    remarks                                  = models.TextField(blank=True, null=True)
    is_rel_qn                                = models.BooleanField(default=False)
    related_qn                               = models.TextField(blank=True, null=True)
    created_at                               = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table  = 'tbl_survey_feedback'
        managed   = True
       
class SurveyFormQuestionnaire(models.Model):
    questionnaire_field                      = models.TextField(default=None, null=True, blank=True)
    questionnaire_value                      = models.TextField(default=None, null=True, blank=True)
    questionnaire_type                       = models.IntegerField(blank=True, null=True, default=0)
    
    class Meta:
        managed = False
        db_table = 'tbl_survey_questionnaire'
