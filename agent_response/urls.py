from django.urls import path, include
from .views import *

urlpatterns = [
    path('get_acwise_summary/',Acwise_summary.as_view()),
    path('get_hourly_graph/',SummaryHourlyGraph.as_view()),
    path('get_datewise_graph/',SummaryDateWiseGraph.as_view()),
    path('get_agent_forms/',Get_Agent_forms.as_view()),
    path('get_details/',Get_Details.as_view()),
    path('get_agent_performance/',GetAgentPerformance.as_view()),
    path('gethourlydata/',HourlySummary.as_view()),
    path('getformdetails/',SurveyFormDetails.as_view()),
    path('get_qc_performance/',QC_Performance.as_view()),
    path('get_questions/',GetQuestions.as_view()),
    path('submit_feedback/',SubmitFeedback.as_view()),
    path('get_feedback/',GetFeedback.as_view()),
    path('get_agent_by_qc_performance/',Agent_By_QC_Performance.as_view()),
    path('get_qc_listing/',GetQCListing.as_view()),
    path('update_qc_listing/',UpdateQCListing.as_view()),
    path('submitsurvey/',SurveySubmit.as_view()),
    path('get_agentlisting/',Get_AgentListing.as_view()),
    path('get_agentforms/',Get_Agentforms.as_view()),
    path('get_detailedfeedback/',DetailedFeedback.as_view()),
    ]

