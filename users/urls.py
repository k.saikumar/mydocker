from django.urls import path
from .views import *

urlpatterns = [
    path('login/', Login.as_view()),
    # path('tokeninfo/', TokenInfo.as_view()),
    path('resetpasswordlink/',ResetPasswordLink.as_view()),
    path('resetpassword/',UserResetPassword.as_view()),
    path('changepassword/',ChangePassword.as_view()),
    path('register/',RegisterUser.as_view()),
    path('updateuser/',UpdateUser.as_view()),
    path('getchips/',GetChips.as_view()),
    path('getchipsdata/',GetChipsData.as_view()),
    path('getroles/',GetRoles.as_view()),
    path('updateuserstatus/',UpdateUserStatus.as_view()),
    path('getdistricts/',GetDistricts.as_view()),
    path('getacs/',GetAcs.as_view()),
    path('getsurveyors/',GetSurveyors.as_view()),
    path('getbooths/',GetBoothList.as_view()),
    path('getelectoraldata/',GetElectoralList.as_view()),
    path('getuserinfo/',GetUserInfo.as_view()),
    
    path('get_education/',GetEducation.as_view()),
    path('get_category/',GetCategory.as_view()),
    path('get_religion/',GetReligion.as_view()),
    path('get_party_2019/',GetPartyLS2019.as_view()),
    path('get_party_upcoming_election/',GetPartyUpcoming.as_view()),
    path('get_chief_minister_new/',GetCMNew.as_view()),
    path('get_occupation/',GetOccupations.as_view()),
    path('getcaste/',GetCastes.as_view()),
    path('getcandidate/',GetCandidates.as_view()),
    path('get_schemes/',GetSchemes.as_view()),
    

]