from .models import *
from rest_framework import serializers
import string,random
from agent_response.models import *
import bcrypt 

# def set_password(self, raw_password):
#         # Your custom implementation for setting password
#         # Example: You can hash the password using bcrypt
#         self.password = bcrypt.hashpw(raw_password.encode('utf-8'), bcrypt.gensalt())

# def check_password(self, raw_password):
#     # Your custom implementation for checking password
#     # Example: You can compare hashed passwords using bcrypt
#     return bcrypt.checkpw(raw_password.encode('utf-8'), self.password.encode('utf-8'))

def encrypt(email):
    encrypted_email = ""
    key=42
    for char in email:
        encrypted_email += chr((ord(char) + key) % 256)
    return encrypted_email

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # fields = ('email','name','emp_code','user_role','district','ac')
        fields = ('fullname','phone_number','email','role','ac_name','assigned_agent_id','district','mapped_booths')
        
    def create(self,validated_data):
        password = '123456'
        encrypted_password = encrypt(password)
        user = User(
            fullname                    = validated_data['fullname'],
            phone_number                = validated_data['phone_number'],
            email                       = validated_data['email'],
            role                        = validated_data['role'],
            ac_name                     = validated_data['ac_name'],
            assigned_agent_id           = validated_data['assigned_agent_id'],
            district                    = validated_data['district'],
            password                    = encrypted_password,
            mapped_booths               = validated_data['mapped_booths']
        )
        # user.set_password(validated_data['password'])
        # user.password = validated_data['password']
        # password = (''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=8)))
        # user.set_password(''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=8)))
        # user.set_password(password)
        user.save()
        return user

class EditUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class EmailLoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()

class ResetPasswordLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResetPassword
        fields = ('user_id','key','time')

class ResetPasswordsSerializer(serializers.Serializer):
    password = serializers.CharField()
    confirm_password = serializers.CharField()
    key = serializers.CharField(max_length= 25)
