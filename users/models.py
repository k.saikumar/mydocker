from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone
# from agent_response.models import User

class Roles(models.Model):
    role_name = models.CharField(max_length=25,null=True,blank=True)
    def __str__(self):
        return self.role_name
    
class Users(AbstractBaseUser):
    name            = models.CharField(max_length=25, null=False, blank=False, default=True)
    email           = models.EmailField(max_length=50, null=False, blank=False, unique=True)
    password        = models.CharField(max_length=1024, null=True, blank=True)
    emp_code        = models.CharField(max_length=10, null=True, unique=True)
    user_role       = models.ForeignKey(Roles, on_delete=models.CASCADE)
    district        = models.CharField(max_length=30, null=True)
    ac              = models.CharField(max_length=30, null=True)
    is_active       = models.BooleanField(default=True)
    registered_on   = models.DateTimeField(auto_now_add=True,null = True)
    
    USERNAME_FIELD = 'email'

    def _str_(self):
        return self.email
    
class User(models.Model):
    fullname                        = models.CharField(max_length=150, blank=True, null=True)
    phone_number                    = models.CharField(max_length=150, blank=True, null=True,unique=True)
    email                           = models.CharField(max_length=255, blank=True, null=True)
    password                        = models.CharField(max_length=1024, blank=True, null=True)
    emp_code                        = models.CharField(max_length=200, blank=True, null=True)
    role                            = models.IntegerField(blank=True, null=True, default=0)
    ac_name                         = models.CharField(max_length=200, blank=True, null=True)
    otp                             = models.IntegerField(blank=True, null=True, default=0)
    opt_datetime                    = models.DateTimeField(blank=True, null=True)
    status                          = models.IntegerField(blank=True, null=True, default=1)
    registered_on                   = models.DateTimeField(default=timezone.now,blank=True, null=True)
    assigned_agent_id               = models.CharField(max_length=1024, blank=True, null=True)
    district                        = models.CharField(max_length=200, blank=True, null=True)
    mapped_booths                   = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_users'

    
class ResetPassword(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    key  = models.CharField(max_length=25,null=True,blank=True)
    time = models.DateTimeField(auto_now_add=False)


#Districts Model
class Districts(models.Model):
    district_id                     = models.AutoField(primary_key=True)
    state_id                        = models.IntegerField(blank=True, null=True, default=0)
    district_code                   = models.IntegerField(blank=True, null=True, default=0)
    district_display_code           = models.CharField(max_length=50, blank=True, null=True)
    district_name                   = models.CharField(max_length=255, blank=True, null=True)
    district_name_tn                = models.CharField(max_length=255, blank=True, null=True)
    user_count                      = models.IntegerField(blank=True, null=True, default=0)
    
    class Meta:
        managed = False
        db_table = 'tbl_districts'

#Ac Model
class AcList(models.Model):
    id                              = models.AutoField(primary_key=True)
    state                           = models.CharField(max_length=200, blank=True, null=True)
    district                        = models.CharField(max_length=200, blank=True, null=True)
    ac                              = models.CharField(max_length=200, blank=True, null=True)
    ac_tn                           = models.CharField(max_length=100, blank=True, null=True)
    ac_number                       = models.CharField(max_length=100, blank = True, null =True)
    
    class Meta:
        managed = False
        db_table = 'tbl_ac'

#Municipal Corp Model
class MunicipalCorp(models.Model):
    id                              = models.AutoField(primary_key=True)
    district_id                     = models.IntegerField(blank=True, null=True, default=0)
    municipal_corp_name             = models.CharField(max_length=255, blank=True, null=True)
    municipal_corp_name_tn          = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'tbl_municipal_corp'

#BlockList Model
class BlockList(models.Model):
    id                              = models.AutoField(primary_key=True)
    district_id                     = models.IntegerField(blank=True, null=True, default=0)
    block                           = models.CharField(max_length=255, blank=True, null=True)
    block_tn                        = models.CharField(max_length=100, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'tbl_block'


#GpanchayatList Model
class GpanchayatList(models.Model):
    id                              = models.AutoField(primary_key=True)
    block_id                        = models.IntegerField(blank=True, null=True, default=0)
    panch_name                      = models.CharField(max_length=255, blank=True, null=True)
    panch_name_tn                   = models.CharField(max_length=100, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'tbl_gram_panchayat'


#VillageList Model
class VillageList(models.Model):
    id                              = models.AutoField(primary_key=True)
    panch_id                        = models.IntegerField(blank=True, null=True, default=0)
    village_name                    = models.CharField(max_length=255, blank=True, null=True)
    village_name_tn                 = models.CharField(max_length=100, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'tbl_village'

class BoothList(models.Model):
    ac_name                         =   models.CharField(max_length=255, blank=True, null=True)
    ac_number                       =   models.CharField(max_length=100, blank=True, null=True)
    booth_name                      =   models.CharField(max_length=255, blank=True, null=True)
    booth_number                    =   models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'tbl_ac_booth'

class ElectoralList(models.Model):
    district                       =   models.CharField(max_length=255, blank=True, null=True)
    ac_name                        =   models.CharField(max_length=255, blank=True, null=True)
    ac_number                      =   models.CharField(max_length=100, blank=True, null=True)
    booth_number                   =   models.CharField(max_length=10,null=True,blank=True)
    booth_name                     =   models.CharField(max_length=255,null=True,blank=True)
    house_number                   =   models.CharField(max_length=10,null=True,blank=True)
    name                           =   models.CharField(max_length=255, blank=True, null=True)
    age                            =   models.CharField(max_length=10, blank=True, null=True)
    gender                         =   models.CharField(max_length=50, blank=True, null=True)
    address                        =   models.TextField(null=True,blank=True)
    age_category                   =   models.CharField(max_length=255, blank=True, null=True)
    survey_status                  =   models.IntegerField(default=0)
    epic_id                        =   models.CharField(max_length=255, blank=True, null=True)
    surveyed_by                    =   models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    surveyed_timestamp             =   models.DateTimeField(null=True,blank=True)
    is_found                       =   models.IntegerField(default=0)


    class Meta:
        managed = True
        db_table = 'tbl_electoral_data'

class GovtSchemes(models.Model):
    scheme                      = models.CharField(max_length=255,null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'ap_gov_schemes'


