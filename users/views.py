import jwt
from django.conf import settings
from .models import *
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from django.contrib.auth.hashers import check_password
from datetime import datetime, timedelta
# import openpyxl
from django.core.mail import send_mail
from django.template.loader import render_to_string
import random,string
from django.utils import timezone 
from botocore.exceptions import ClientError
import boto3

def validatedate(date_text):
    try:
        datetime.strptime(date_text,'%Y-%m-%d')
        return True
    except :
        return False

def decrypt(encrypted_email):
    decrypted_email = ""
    key=42
    for char in encrypted_email:
        decrypted_email += chr((ord(char) - key) % 256)
    return decrypted_email


# def generate_token(name,email,password,emp_code,user_role_id,district,ac):
def generate_token(fullname,phone_number,email,password,role,ac_name,assigned_agent_id,district):
    payload = {
        'fullname' : fullname,
        'phone_number':phone_number,
        'email' : email,
        'password': password,
        'role'  : role,
        'ac_name'        : ac_name,
        'assigned_agent_id':assigned_agent_id,
        'district' : district,
        'exp': datetime.utcnow() + timedelta(hours=24),
    }
    token = jwt.encode(payload, settings.JWT_SECRET_KEY, algorithm='HS512')
    return token

def handle_auth_exceptions(func):
    def wrapper(*args, **kwargs):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            request = args[1]  # Assuming the request is the second argument
            header_access_token = request.META.get('HTTP_AUTHORIZATION')
            if not header_access_token:
                result['result']['message'] = "Authorization header missing"
                return Response(result,status=status.HTTP_400_BAD_REQUEST)
            splited_access_token = header_access_token.split(' ')
            if len(splited_access_token) != 2:
                result['result']['message'] = "Authorization header missing"
                return Response(result,status=status.HTTP_400_BAD_REQUEST)
            access_token = splited_access_token[1]
            user_data = jwt.decode(access_token, settings.JWT_SECRET_KEY, algorithms=["HS512"])
            if not user_data or 'email' not in user_data:
                result['result']['message'] = "User data not found in token"
                return Response(result,status=status.HTTP_401_UNAUTHORIZED)
            request.user_data = user_data
            return func(*args, **kwargs)
        except jwt.ExpiredSignatureError:
            result['result']['message'] = "Token expired. Please login again"
            return Response(result, status=status.HTTP_401_UNAUTHORIZED)
        except User.DoesNotExist:
            result['result']['message'] = "User not found"
            return Response(result, status=status.HTTP_404_NOT_FOUND)
        except ValueError as e:
            result['result']['message'] = str(e)
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            result['result']['message'] = str(e)
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return wrapper

class RegisterUser(APIView):
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}

        email = request.data['email']
        phone_number = request.data['phone_number']
        role = request.data['role']

        if role != "5" and len(email)>0:
            email_data = User.objects.filter(email=email)
            if len(email_data)>0:
                result['result'] = {"message":"User with this email already exists","data" :{}}
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        elif role == "5" and len(phone_number)>0:
            phone_data = User.objects.filter(phone_number = phone_number)
            if len(phone_data)>0:
                result['result'] = {"message":"User with this phone number already exists","data" :{}}
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)



            
        serializer       = UserSerializer(data = request.data)
        if serializer.is_valid():
            password = (''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=8)))
            password = '123456'
            serializer.validated_data['password'] = password
            serializer.save()
            name = request.data['fullname']
            email = request.data['email']
            # msg_plain = ''  
            # msg_html = render_to_string(
            #     'users/create_user.html', {'name': name, 'username': email, 'password': password})
            # send_mail('Survey Analysis Dashboard', msg_plain,
            #             'info@pkconnect.com', [email], html_message=msg_html,)   
            # except Exception as e :
            #     result['status'] = 'NOK'
            #     result['valid'] = False
            #     result['result']['message'] = "Error in sending mail"
            #     result['result']['message'] = str(e)
            #     return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                
            result['status']    =   "OK"
            result['valid']     =   True
            result['result']['message'] =   "User registered successfully"
            result['result']['data'] = serializer.data
            return Response(result,status=status.HTTP_200_OK)

        else:
            result['result']['message'] = (list(serializer.errors.keys())[
                0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

class Login(APIView):
    def post(self,request):
        result = {}
        result['status']    =   "NOK"
        result['valid']     =   False
        result['result']    =   {"message":"Unauthorized access","data":{}}
        serializer = EmailLoginSerializer(data = request.data)
        if serializer.is_valid():
            email           = serializer.validated_data['email']
            password        = serializer.validated_data['password']
            try:
                user_data       = User.objects.get(email=email)
            except Exception as e:
                    result['result']['message'] = "Invalid Username or Password"
                    return Response(result, status=status.HTTP_401_UNAUTHORIZED)
            # if (user_data is None) or (not user_data.is_active) or (user_data.check_password(password)== False):
            if (user_data is None) or (not user_data.status):
                result['result']['message'] = "Invalid username or password"
                return Response(result, status=status.HTTP_401_UNAUTHORIZED)
            else:
                decrypted_password = decrypt(user_data.password)
                if decrypted_password != password:
                    result['result']['message'] = "Incorrect password"
                    return Response(result, status=status.HTTP_401_UNAUTHORIZED)
                token                       =   generate_token(user_data.fullname,user_data.phone_number,user_data.email,user_data.password,user_data.role,user_data.ac_name,user_data.assigned_agent_id,user_data.district)
                final_response  =   {
                            'name'          : user_data.fullname,
                            'user_role'     : user_data.role,
                            'last_login'    : (datetime.today()).strftime('%Y-%m-%d %H:%M:%S'),
                            'user_id'       : user_data.id,
                            'district'      : user_data.district,
                            'ac'            : user_data.ac_name,
                            'email'         : user_data.email,
                            'phone_number'  : user_data.phone_number,
                            'assigned_agents' : user_data.assigned_agent_id,
                            'token'         : token,
                        }

                result['status']            =   "OK"
                result['valid']             =   True
                result['result']['message'] =   "You are successfully logged in."
                result['result']['data']    =   final_response
                return Response(result,status=status.HTTP_200_OK)
                    
        else:
            result['result']['message'] = (list(serializer.errors.keys())[
                0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

class UpdateUser(APIView):
    @handle_auth_exceptions
    def post(self, request, format=None):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            user = User.objects.get(id=request.data['id'])
            serializer = EditUserSerializer(
                user, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                result['status'] = "OK"
                result['valid'] = True
                result['result']['message'] = "User updated successfully!"
                return Response(result, status=status.HTTP_200_OK)
            else:
                result['result']['message'] = (list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


#   USER PASSWORD CHANGE, RESET

class ResetPasswordLink(APIView):
    serializer_class = ResetPasswordLinkSerializer
    def post(self, request, format=None):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}
        key = ''.join(random.choices(
            string.ascii_uppercase + string.digits, k=20))
        serializer_data = dict()
        serializer_data['key'] = key
        serializer_data['time'] = datetime.now()

        user_data = User.objects.all().filter(email=request.data['email'])

        if len(user_data) == 0:
            result['result']['message'] = 'Your email address is not registered with us.'
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        else:
            user_id = user_data.values()[0]['id']

            try:
                prev_data = ResetPassword.objects.filter(user_id_id=user_id)
                len_prev = len(prev_data.values())
                if len_prev > 0:
                    serializer_data = {}
                    serializer_data['key'] = key
                    serializer_data['time'] = datetime.now()
                    # serializer_data['user_id']  = user_id
                    serializer = ResetPasswordLinkSerializer(ResetPassword.objects.get(
                        user_id_id=user_id), data=serializer_data, partial=True)

                    if serializer.is_valid():
                        serializer.save()
                    else:
                        result['result']['message'] = (list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
                        return Response( result,status=status.HTTP_200_OK)

                else:

                    serializer_data = {}
                    serializer_data['key'] = key
                    serializer_data['time'] = datetime.now()
                    serializer_data['user_id'] = user_id

                    serializer = ResetPasswordLinkSerializer(data=serializer_data)

                    if serializer.is_valid():
                        serializer.save()
                    else:
                        result['result']['message']=(list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
                        return Response(result, status=status.HTTP_200_OK)
                    
            except Exception as e:
                result['result']['message'] = str(e)
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            data = "https://voteworks.in/stage/survey-dashboard/#/auth/create-password/"+key
            # data = "http://localhost:4200/#/auth/create-password/"+key
            email = request.data['email']
            obj = Users.objects.filter(email=email).values()
            obj = obj[0]['name']
            msg_plain = ''
            msg_html = render_to_string(
                'users/resetpass.html', {'link': data, 'obj': obj})
            
            send_mail('Reset Password request', msg_plain, 'info@pkconnect.com',
                      [request.data['email']], html_message=msg_html,)
        except:
            result['result']['message']="Error while sending mail"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # Response data
        result['status']="OK"
        result['valid']=True
        result['result']['message'] = "Reset Password link has been sent to your email address."
        # Response data
        return Response(result , status=status.HTTP_200_OK)

class UserResetPassword(APIView):
    serializer_class = ResetPasswordsSerializer
    def post(self, request, format=None):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}
        if request.data['password'] != request.data['confirm_password']:
            result['result']['message'] = "Both passwords must be same"
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        try:
            user_details = ResetPassword.objects.filter(key=request.data['key']).values('id', 'key', 'time', 'user_id_id__email','user_id_id')
            if len(user_details) == 0:
                result['result']['message'] = "Invalid user"
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception as e:
            result['result']['message'] = str(e)
            return Response(result,status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        email_id = user_details[0]['user_id_id__email']
        start_time = user_details[0]['time'].replace(tzinfo=timezone.utc)
        endtime = datetime.now(timezone.utc)
        duration = (endtime-start_time).total_seconds() / 60.0
        if duration > 20:
            result['status']="OK"
            result['valid']=True
            result['result']['message'] = "Request Time Out!"
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        serializer = ResetPasswordsSerializer(data=request.data)
        try:
            user = Users.objects.get(email=email_id)
        except:
            result['result']['message'] = "Invalid Email Address"
            return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if serializer.is_valid():
            user.set_password(serializer.data['password'])
            user.save()
            obj = Users.objects.filter(email=email_id).values()
            obj = obj[0]['name']
            msg_plain = ''
            msg_html = render_to_string('users/reset.html', {'name': obj})
            send_mail('Password Reset Successful', msg_plain,
                      'info@pkconnect.com', [email_id], html_message=msg_html,)
            prev_data = ResetPassword.objects.get(id=user_details[0]['id'])
            serializer_data = {}
            serializer_data['key'] = ""
            serializer = ResetPasswordLinkSerializer(
                prev_data, data=serializer_data, partial=True)
            if serializer.is_valid():
                serializer.save()
            else:
                result['status']="NOK"
                result['valid']=False
                result['result']['message'] = (list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
                return Response(result, status=status.HTTP_200_OK)
            # Response data
            result['status']="OK"
            result['valid']=True
            result['result']['message'] = "Password reset successfully !"
            # Response data
            return Response(result, status=status.HTTP_200_OK)
        else:
            result['result']['message'] = (list(serializer.errors.keys())[0]+' - '+list(serializer.errors.values())[0][0]).capitalize()
            return Response(result, status=status.HTTP_400_BAD_REQUEST)

class ChangePassword(APIView):
    @handle_auth_exceptions
    def post(self, request, format=None):
        result = {}
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            user_data = request.user_data
            user = Users.objects.get(email=user_data['email'])
            if user.check_password(serializer.validated_data['old_password']):
                user.set_password(serializer.validated_data['new_password'])
                user.save()
                result['status'] = "OK"
                result['valid'] = True
                result['result'] = {"message": "Password Changed"}
                return Response(result, status=status.HTTP_201_CREATED)
            else:
                result['result'] = {"message": "Password did not match"}
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        else:
            result['result'] = serializer.errors
            return Response(result, status=status.HTTP_400_BAD_REQUEST)

class UpdateUserStatus(APIView):
    @handle_auth_exceptions
    def post(self, request, format=None):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            if request.data['ids'] == "":
                result['result']['message'] = "ids cannot be empty"
                return Response(result, status=status.HTTP_401_UNAUTHORIZED)
            else:
                ids = request.data['ids'].split(",")
                qs  = User.objects.filter(id__in=ids)
                
                action = request.data['action']
                newStatus = 0 if action == 'inactive' else 1
                for obj in qs:
                    temp = User.objects.filter(id=obj.id).values()
                    temp.update(status=newStatus)

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "User status changed successfully"
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetChips(APIView):
    @handle_auth_exceptions
    def post(self, request, format=None):
        result              =   {}
        result['status']    =   "NOK"
        result['valid']     =   False
        result['result']    =   {"message":"Unauthorized access","data":{}}
        try:
            filter_object =Q()
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data['start_date']
            end_date    = request.data['end_date']

            if start_date=="" and end_date =="":
                start_date= "2024-01-01"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            if(end_date<start_date):
                result['valid']=True
                result['result']['message']="end date should not be less than start date"
                return Response (result,status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            else:
                end_date=( datetime.strptime(end_date,'%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
            
            filter_object       &=          Q(registered_on__range=(start_date,end_date))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                
            user_data = request.user_data
            search_string = request.data['search_string']

            if 'status' in request.data and request.data['status']:
                    filter_object &= Q(status = request.data['status']) 

            if search_string != '':
                search_condition = (
                    Q(fullname__icontains=search_string) |
                    Q(email__icontains=search_string) |
                    Q(emp_code__icontains=search_string)
                )
                filter_object &= search_condition

            filter_object &= ~Q(email=(user_data['email']))
            data = User.objects.filter(filter_object).values_list('id')
            # all_count = data.count()
            sa_count = data.filter(role=1).count()
            qc_count = data.filter(role=3).count()
            tl_count = data.filter(role=4).count()
            if 'ac_name' in request.data and request.data['ac_name'] != '':
                sy_count = data.filter(role=5,ac_name=request.data['ac_name']).count()
            else:
                sy_count = data.filter(role=5).count()
            all_count = sa_count+qc_count+tl_count+sy_count

            bucket_response = {
                'buckets': [
                    {"bucket_id": "0", "bucket_name": "All", "value": all_count},
                    {"bucket_id": "1", "bucket_name": "Super Admin", "value": sa_count},
                    {"bucket_id": "3", "bucket_name": "QC Agents", "value": qc_count},
                    {"bucket_id": "4", "bucket_name": "Team Lead", "value": tl_count},
                    {"bucket_id": "5", "bucket_name": "Surveyor", "value": sy_count},
                ]
            }
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Chips listing fetched successfully"
            result['result']['data'] = bucket_response
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetChipsData(APIView, PageNumberPagination):
    @handle_auth_exceptions
    def post(self, request, format=None):
        result = {}
        result['status']    =   "NOK"
        result['valid']     =   False
        result['result']    =   {"message":"Unauthorized access","data":{}}
        try:
            filter_object =Q()
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data['start_date']
            end_date    = request.data['end_date']

            if start_date=="" and end_date =="":
                start_date= "2024-01-01"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            if(end_date<start_date):
                result['valid']=True
                result['result']['message']="end date should not be less than start date"
                return Response (result,status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            else:
                end_date=( datetime.strptime(end_date,'%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
            
            filter_object       &=          Q(registered_on__range=(start_date,end_date))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if 'status' in request.data and request.data['status']:
                filter_object &= Q(status = request.data['status']) 

            user_data = request.user_data
            bucket_id = request.data['bucket_id']
            search_string = request.data['search_string']
            if bucket_id == "":
                result['result']['message'] = "Bucket ID cannot be empty"
                return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            if search_string != '':
                search_condition = (
                    Q(fullname__icontains=search_string) |
                    Q(email__icontains=search_string) |
                    Q(phone_number__icontains=search_string)
                )
                filter_object &= search_condition
            
            if bucket_id == '5' and ('ac_name' in request.data and request.data['ac_name'] != ''):
                ac_condition = Q(ac_name=request.data['ac_name'])
                filter_object &= ac_condition

            filter_object &= ~Q(email=(user_data['email']))
            if bucket_id != '0':
                filter_object &= Q(role=bucket_id)
            data = User.objects.filter(filter_object).values('id','fullname','email', 'status', 'role' ,'district','ac_name', 'assigned_agent_id','phone_number','mapped_booths').order_by('-id')
            for record in data:
                role = record['role']
                if role == 1:
                    role_name = 'Super Admin'
                elif role == 3:
                    role_name = 'QC Agent'
                elif role == 4:
                    role_name = 'TC'
                elif role == 5:
                    role_name = 'Surveyor'
                else:
                    role_name = 'Unknown Role'
                record['role_name']  = role_name 
            paginated_data = self.paginate_queryset(data, request, view=self)
            paginated_data = self.get_paginated_response(paginated_data).data
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['next'] = paginated_data['next']
            result['result']['previous'] = paginated_data['previous']
            result['result']['count'] = paginated_data['count']
            result['result']['data'] = paginated_data['results']
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetRoles(APIView):
    # @handle_auth_exceptions
    def get(self, request, format=None):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            data = Roles.objects.all().values()
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully!"
            result['result']['data'] = data
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetDistricts(APIView):
    def get(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            districts_data = Districts.objects.all().values('district_id','district_name','district_code')
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully!"
            result['result']['data'] = districts_data
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetAcs(APIView):
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            districts = request.data.get('district')

            if districts is not None:

                if isinstance(districts, str):
                    districts = districts.split(',')
                query = Q()
                for district in districts:
                    query |= Q(district=district)

                acs_data = AcList.objects.filter(query).values()

            else:
                acs_data = AcList.objects.all().values()

                
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully!"
            result['result']['data'] = acs_data
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetSurveyors(APIView):
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            qc_data = User.objects.filter(role = 3, status =1).values('assigned_agent_id')
            assigned_agent_ids = set()
            for response in qc_data:
                assigned_agent_id = response['assigned_agent_id']
                if assigned_agent_id is not None and assigned_agent_id:
                    agent_ids = [int(agent_id) for agent_id in assigned_agent_id.split(',')]

                    
                    assigned_agent_ids.update(agent_ids)

            

            if 'user_id' in request.data and request.data['user_id']!='':
                user_id = request.data['user_id']
                current_agent_ids = User.objects.filter(id=user_id).values('assigned_agent_id').first()
                if current_agent_ids and current_agent_ids['assigned_agent_id']:
                    current_agent_ids = {int(agent_id) for agent_id in current_agent_ids['assigned_agent_id'].split(',')}

                    
                    assigned_agent_ids.difference_update(current_agent_ids)



            common_queryset = User.objects.filter(role=5, status=1)
            if 'filter_flag' in request.data and request.data['filter_flag'] == '1':
                surveyors_data = common_queryset.values('id', 'fullname')
            else:
                surveyors_data = common_queryset.exclude(id__in=assigned_agent_ids).values('id', 'fullname')
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully!"
            result['result']['data'] = surveyors_data
            return Response(result, status=status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
class GetBoothList(APIView):
    def post(self,request):
        filter_obj = Q()
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            filter_obj &= Q(ac_name = request.data['ac_name'])
        
        booth_data = BoothList.objects.filter(filter_obj).values()
        result = {}
        result['status'] = 'OK'
        result['valid']  =  True
        result['result'] = {"message":"Booth list fetched successfully","data" :booth_data}
        return Response(result,status=status.HTTP_200_OK)
    

class GetElectoralList(APIView):
    def post(self,request,format=None):
        filter_obj = Q()
        survey_status = request.data['survey_status']
        filter_obj &= Q(survey_status = survey_status)
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            ac_name = request.data['ac_name']
            filter_obj &= Q(ac_name = ac_name)
        if 'booth_number' in request.data and request.data['booth_number'] != '':
            booth_number = request.data['booth_number']
            filter_obj &= Q(booth_number = booth_number)

        if 'search_string' in request.data and request.data['search_string'] != '':
            search_string = request.data['search_string']
            search_condition = (Q(name__icontains=search_string)| Q(house_number__icontains=search_string))
            filter_obj &= search_condition
        
        electoral_data = ElectoralList.objects.filter(filter_obj).values()
        # paginated_data = self.paginate_queryset(electoral_data, request, view=self)
        # paginated_data = self.get_paginated_response(paginated_data).data
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'Electoral list fetched successfully'}
        # result['result']['next'] = paginated_data['next']
        # result['result']['previous'] = paginated_data['previous']
        # result['result']['count'] = paginated_data['count']
        result['result']['data'] = electoral_data
        return Response(result,status=status.HTTP_200_OK)
    
class GetUserInfo(APIView):
    def post(self,request):
        user_id = request.data['user_id']
        userdata = User.objects.filter(id=user_id).values('fullname','phone_number','email','role','ac_name','assigned_agent_id','district','mapped_booths')
        booth_queryset = userdata.values_list('mapped_booths', flat=True)
        queryset  = BoothList.objects.filter( ac_name = userdata[0]['ac_name'], booth_number__in = set(','.join(map(str, booth_queryset)).split(','))).values('booth_number', 'booth_name')
        booth_list = []

        for entry in queryset:
            booth_list.append({
                'booth_name' : entry['booth_name'],
                'booth_number' : entry['booth_number'],
            })

        result_dict = {
            'fullname' : userdata[0]['fullname'],
            'phone_number' : userdata[0]['phone_number'],
            'email' : userdata[0]['email'],
            'role' : userdata[0]['role'],
            'ac_name' : userdata[0]['ac_name'],
            'assigned_agent_id' : userdata[0]['assigned_agent_id'],
            'district' : userdata[0]['district'],
            'mapped_booths' : userdata[0]['mapped_booths'],
            'mapped_booth_names' : booth_list,
        }

        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message': 'User information fetched successfully.','data':[result_dict]}
        return Response(result,status=status.HTTP_200_OK)
    
# class GetOccupation(APIView):
#     def post(self,request):
#         occupation_list = [
#             'Unskilled manual worker (Artisans/ bricklayers/ potters/ stone cutters/ furniture/basketry/ matmakers',
#             'Skilled manual worker (Driver/ Mechanic/ Electrician/ Plumber/ Gold Smith/ Tailor/Cobbler/ Carpenter/ Sailor)',
#             'Businessman: No. of employees: 0',

#         ]

class GetEducation(APIView):
    def get(self,request):
        list_data = ["No Education","Below 8th Class","Between 8th to 10th Class","Between 10th to 12 Class","Diploma","Undergraduate","Post Graduate","Other Education Level","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message': 'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)
    
class GetCategory(APIView):
    def get(self,request):
        list_data = ["GENERAL","OBC","SC","ST","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)
 
class GetReligion(APIView):
    def get(self,request):
        list_data = ["Hindu","Sikh","Jain","Buddhism","Christian","Muslim","Other Religion","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)
 
class GetPartyLS2019(APIView):
    def get(self,request):
        list_data = ["(BSP) Bahujan Samaj Party","(TDP) Telugu Desam Party","(BJP) Bharatiya Janata Party","(INC) Indian National Congress","(CPI(M)) Communist Party of India (Marxist)","(NOTA) None of the above","(CPI) Communist Party of India","(IND) Independent Candidate","(AIMIM) All India Majlis-E-Ittehadul Muslimeen","(JP) JanaSena Party","(TRS) Telangana Rashtra Samithi","(YSRCP) Yuvajana Sramika Rythu Congress Party","(AAP) Aam Aadmi Party","Other Political Party","Do not want to answer","Did not vote","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)
    
class GetPartyUpcoming(APIView):
    def get(self,request):
        list_data = ["(BSP) Bahujan Samaj Party","(TDP) Telugu Desam Party","(BJP) Bharatiya Janata Party","(INC) Indian National Congress","(CPI(M)) Communist Party of India (Marxist)","(NOTA) None of the above","(CPI) Communist Party of India","(IND) Independent Candidate","(AIMIM) All India Majlis-E-Ittehadul Muslimeen","(JP) JanaSena Party","(TRS) Telangana Rashtra Samithi","(YSRCP) Yuvajana Sramika Rythu Congress Party","(AAP) Aam Aadmi Party","Other Political Party","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)
 
class GetPartyAEToday(APIView):
    def get(self,request):
        list_data = ["(CPI)  Communist Party of India","(TRS)  Telangana Rashtra Samithi","(BJP)  Bharatiya Janata Party","(AIMIM)  All India Majlis-E-Ittehadul Muslimeen","(BSP)  Bahujan Samaj Party","(IND)  Independent Candidate","(CPI(M))  Communist Party of India (Marxist)","(YSRCP) Yuvajana Sramika Rythu Congress Party","(AAP)  Aam Aadmi Party","(TDP)  Telugu Desam Party","(JP) JanaSena Party","(INC)  Indian National Congress","(AAP) Aam Aadmi Party","Other Political Party","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)

class GetCMNew(APIView):
    def get(self,request):
        list_data = ["Any BJP Candidate","Kanna Lakshminarayana","Somu Veerraju","Pawan Kalyan","Any INC Candidate","Sake Sailajanath","Dharmana Krishna Das","Acham Naidu","Any YSRCP Candidate","Kiran Kumar Reddy","Y.S. Vijayamma","Any TDP Candidate","Y. S. Jagan","N. Chandrababu Naidu","Nara Lokesh","Any JSP Candidate","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)

class GetOccupations(APIView):
    def get(self,request):
        list_data = [
"Unskilled manual worker (Artisans/ bricklayers/ potters/ stone cutters/ furniture/ basketry/ matmakers)",
"Skilled manual worker (Driver/ Mechanic/ Electrician/ Plumber/ Gold Smith/ Tailor/ Cobbler/ Carpenter/ Sailor)",
"Businessman: No. of employees: 0",
"Businessman: No. of employees: 1-5",
"Businessman: No. of employees: 6-10",
"Businessman: No. of employees: 10+",
"Self-employed professional (Lawyer/Insurance Agent/ Free Lancer etc)",
"Supervisor",
"Clerk/Salesman",
"Officer/Exec - Junior",
"Officer/Exec - Middle/Senior",
"Owner Cultivator: greater-than 2.5 acres",
"Owner Cultivator: 2.5 - 5 acres",
"Owner Cultivator: 6 - 10 acres",
"Owner Cultivator: 11 - 25 acres",
"Owner Cultivator: 25+ acres",
"Tenant / Non-owner cultivator",
"Agricultural worker",
"Unemployed",
"Student",
"Housewife",
"Retired",
"Other Occupation",
"Do not want to answer",
"Do not know",
"Respondent Quit"
]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)

class GetCastes(APIView):
    def get(self,request):
        list_data = ["Muslim","Vadabalija","Kapu","Mala","Koppulavelamas","Yadava","Thurpukapus","Gavara","Yata","Other Caste","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)



class GetCandidates(APIView):
    def get(self,request):
        list_data = ["Appalanarasimharaju Botcha (YSRCP)","Bharath Bhushan Reddy (BJP)","Bheemsetty Prabhakar (TDP)","Koka Venkata Appalaraju (INC)","Yeluri Sambasiva Rao (AAP)","Kottam Ram Mohan Rao (JSP)","Meka Venkata Appalaraju (TRS)","Panchakarla Ramesh Babu (CPI)","Penmetsa Vishnu Kumar Raju (IND)","Pesala Ravindra Kumar (AIMIM)","Chalamalasetty Sunil Kumar (CPI(M))","Chintalapudi Venkataramayya (BSP)","Vishnu Kumar Raju Penmetsa (JSP)","Rama Raju Yalamarthi (INC)","Other Candidates","Do not want to answer","Do not know answer","Respondent Quit"]
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)


class GetSchemes(APIView):
    def get(self,request):
        scheme_data = GovtSchemes.objects.all().values('scheme')
        list_data = []
        for sd in scheme_data:
            list_data.append(sd['scheme'])
        
        list_data.extend(["Other Schemes", "Do not know", "Do not want to answer", "Respondent Quit"])
        result = {}
        result['status'] = 'OK'
        result['valid'] = True
        result['result'] = {'message':'List fetched successfully','data':list_data}
        return Response(result,status=status.HTTP_200_OK)


