# Use the official Python image as a base
FROM python:3.10

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements.txt file first to leverage Docker cache
COPY . /app

# Install dependencies
# RUN apt-get update \
#     && apt-get install -y --no-install-recommends \
#         build-essential \
#         libpq-dev \
#     && rm -rf /var/lib/apt/lists/*
    
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install pymysql

# Copy the rest of the application code
COPY . .

# Expose port 8000 to the outside world
EXPOSE 9000

# Define the command to run the application
CMD ["./entrypoint.sh"]
