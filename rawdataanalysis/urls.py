from django.urls import path
from .views import * 
urlpatterns = [
    path('getrawdatakpis/',GetRawDataKpis.as_view()),
    path('getrawdatadistributions/',GetRawDataDistributions.as_view()),
    path('getprofilewisesummary/',ProfileWiseSummary.as_view()),
    path('getrawprofilesdata/',GetRawProfiles.as_view()),
    # path('getallpartyvoteshare/',PartyVoteShare.as_view()),
    path('getpartyvoteshare/',PartyVoteShare.as_view()),

    path('get_qc_response/',Get_QC_Response.as_view()),

    path('getacwiseforms/',ACWiseForms.as_view()),
]