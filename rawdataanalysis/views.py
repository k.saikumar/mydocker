from django.shortcuts import render
from rest_framework.views import *
# Create your views here.
from agent_response.models import *
from users.models import *
from django.db.models import Q
from datetime import *
from django.db.models import Sum, Count
from users.views import handle_auth_exceptions
from notifications.models import *

def validatedate(date_text):
    try:
        datetime.strptime(date_text,'%Y-%m-%d')
        return True
    except :
        return False
class GetRawDataKpis(APIView):
    def post(self,request):

        result = {}
        result['valid'] = False
        result['status'] = 'NOK'
        result['result'] = {'message':'Unauthorized access','data':{}}

        filter_object = Q()
        filter_object1 = Q()
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            ac_condition = Q(ac_name = request.data['ac_name'])
            filter_object &= ac_condition

            userQueryset = User.objects.filter(ac_name = request.data['ac_name'],role=5).values_list('id', flat=True)
            filter_object1 &= Q(user_id__in =list(userQueryset))

        if 'booth_number' in request.data and request.data['booth_number'] != '':
            booth_condition = Q(booth_number = request.data['booth_number'])
            filter_object &= booth_condition 

            getac = BoothList.objects.filter(booth_number = request.data['booth_number']).values_list('ac_name', flat=True).distinct()
            userQueryset1 = User.objects.filter(ac_name__in = list(getac),role=5).values_list('id', flat=True)

            filter_object1 &= Q(user_id__in =list(userQueryset1))
        
        start_date  = request.data['start_date']
        end_date    = request.data['end_date']

        if start_date=="" and end_date =="":
            start_date= "2024-01-01"
            end_date=(datetime.today()).strftime('%Y-%m-%d')

        elif (start_date!="" and end_date==""):
            end_date=(datetime.today()).strftime('%Y-%m-%d')

        if not validatedate(start_date):
            result['valid']=True
            result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
    
        if not validatedate(end_date):
            result['valid']=True
            result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

        start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
        end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)


        today = datetime.today().date()
        total_target_response = ElectoralList.objects.filter(filter_object).count()
        total_response_submitted = ElectoralList.objects.filter(filter_object,surveyed_timestamp__range=(start_date_new, end_date_new),survey_status = 1).count()
        today_response_submitted = ElectoralList.objects.filter(filter_object,surveyed_timestamp__date=today).count()

        if 'start_date' in request.data and request.data['start_date'] != '' and 'end_date' in request.data and request.data['end_date'] != '' :
            filter_object1 &= Q(punch_time__range = (start_date_new, end_date_new))
        else :
            filter_object1 &= Q(punch_time__date = datetime.today())
        attQueryset  = UserAttendance.objects.filter(filter_object1, punch_status = 0).values('user_id').distinct().count()
        final_response = {}
        final_response['total_target_response'] = total_target_response
        final_response['total_response_submitted'] = total_response_submitted
        final_response['today_response_submitted'] = today_response_submitted
        final_response['today_active_agents'] = attQueryset

        result['status'] = 'OK'
        result['valid']  = True
        result['result']['message'] = 'Kpi data fetched successfully'
        result['result']['data']    = final_response

        return Response(result,status=status.HTTP_200_OK)

class GetRawDataDistributions(APIView):
    def post(self,request):
        result = {}
        result['valid'] = True
        result['status'] = 'OK'
        result['result'] = {'message':'Unauthorized access','data':{}}

        filter_object = Q()
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            ac_condition = Q(ac_name = request.data['ac_name'])
            filter_object &= ac_condition

        if 'booth_number' in request.data and request.data['booth_number'] != '':
            booth_condition = Q(booth_number = request.data['booth_number'])
            filter_object &= booth_condition 
        
        start_date  = request.data['start_date']
        end_date    = request.data['end_date']

        if start_date=="" and end_date =="":
            start_date= "2024-01-01"
            end_date=(datetime.today()).strftime('%Y-%m-%d')

        elif (start_date!="" and end_date==""):
            end_date=(datetime.today()).strftime('%Y-%m-%d')
        
        if not validatedate(start_date):
            result['valid']=True
            result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
    
        if not validatedate(end_date):
            result['valid']=True
            result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

        start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
        end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
        timestamp_filter  = (Q(surveyed_timestamp__range=(start_date_new, end_date_new)) | Q(surveyed_timestamp__isnull=True))
        
        total_target_response = ElectoralList.objects.filter(filter_object).count()
        total_response_submitted = ElectoralList.objects.filter(filter_object,surveyed_timestamp__range=(start_date_new, end_date_new),survey_status = 1).count()
        
        final_response = []
        table1 = {}
        table1['tbl_name'] = 'AgeWise Distribution'
        agewisedata = ElectoralList.objects.filter(filter_object).values('age_category').annotate(total_target=Count('id'),total_response_achieved=Count('id',filter=(models.Q(survey_status = 1) & timestamp_filter)))
        age_tbl_data = distribution_dataformat(agewisedata,'age_category',total_target_response,total_response_submitted)
        table1['tbl_data'] = age_tbl_data
        final_response.append(table1)

        default_values = {'target': 0, 'target_percent': 0, 'achieved': 0, 'achieved_percent': 0, 'achieved_deficit_percent': 0, 'target_achieved_percent': 0, 'target_deficit_percent': 0}
        result_age_list = [] 
        age_ranges = [ (18, 25, '18-25'), (26, 35, '26-35'), (36, 50, '36-50'), (51, 98, 'Over_50')]
        for _, _, range_label in age_ranges:
            data = {'row_name': range_label, **default_values}
            age_data = next((entry for entry in age_tbl_data if entry['row_name'] == range_label), None)
            if age_data:
                data.update({
                    'target': age_data.get('target', 0),
                    'target_percent': age_data.get('target_percent', 0),
                    'achieved': age_data.get('achieved', 0),
                    'achieved_percent': age_data.get('achieved_percent', 0),
                    'achieved_deficit_percent': age_data.get('achieved_deficit_percent', 0),
                    'target_achieved_percent': age_data.get('target_achieved_percent', 0),
                    'target_deficit_percent': age_data.get('target_deficit_percent', 0)
                })
            result_age_list.append(data)

        table2 = {}
        table2['tbl_name'] = 'GenderWise Distribution'
        genderwisedata = ElectoralList.objects.filter(filter_object).values('gender').annotate(total_target=Count('id'),total_response_achieved=Count('id',filter=(models.Q(survey_status = 1) & timestamp_filter)))
        gender_tbl_data = distribution_dataformat(genderwisedata,'gender',total_target_response,total_response_submitted)
        table2['tbl_data'] = gender_tbl_data
        final_response.append(table2)


        result_gender_list = [] 
        gender_categories = ['Male', 'Female']

        for gender in gender_categories:
            data = {'row_name': gender, **default_values}
            gender_data = next((entry for entry in gender_tbl_data if entry['row_name'] == gender), None)
            if gender_data:
                data.update({
                    'target': gender_data.get('target', 0),
                    'target_percent': gender_data.get('target_percent', 0),
                    'achieved': gender_data.get('achieved', 0),
                    'achieved_percent': gender_data.get('achieved_percent', 0),
                    'achieved_deficit_percent': gender_data.get('achieved_deficit_percent', 0),
                    'target_achieved_percent': gender_data.get('target_achieved_percent', 0),
                    'target_deficit_percent': gender_data.get('target_deficit_percent', 0)
                })
            result_gender_list.append(data)

        
        result_list = [
            { "tbl_name" : "AgeWise Distribution",     "tbl_data" : result_age_list },
            { "tbl_name" : "GenderWise Distribution",  "tbl_data" : result_gender_list}
        ]
        result['status'] = 'OK'
        result['valid']  = True
        result['result']['message'] = 'Distribution data fetched successfully'
        result['result']['data']    = result_list 

        return Response(result,status=status.HTTP_200_OK)
        

def distribution_dataformat(inputdata,key,total_target_response,total_response_submitted):
    tbl_data = []
    for data in inputdata:
        temp_row = {}
        temp_row['row_name'] = data[key]
        temp_row['target'] = data['total_target']
        target_percent = '0' if total_target_response == 0 else str(round((data['total_target']*100/(total_target_response)),2))
        temp_row['target_percent'] = target_percent + "%"
        temp_row['achieved'] = data['total_response_achieved']
        achieved_percent  = '0' if total_response_submitted == 0 else str(round((data['total_response_achieved']*100/(total_response_submitted)),2)) 
        temp_row['achieved_percent'] = achieved_percent + "%"
        deficit_percent_value = round((float(target_percent) - float(achieved_percent)),2) if float(target_percent) - float(achieved_percent) > 0 else 0
        temp_row['achieved_deficit_percent'] = str(deficit_percent_value) + "%"
        temp_row['target_achieved_percent'] = '0%' if (data['total_target']) == 0 else (str(round((data['total_response_achieved']*100/(data['total_target'])),2)) + "%")
        # temp_row['target_deficit'] = data['total_target'] - data['total_response_achieved']
        temp_row['target_deficit_percent'] = str(round(((data['total_target'] - data['total_response_achieved'])*100/(data['total_target'])),2)) + "%"
        tbl_data.append(temp_row)
    return tbl_data

class ProfileWiseSummary(APIView):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            filter_object = Q()

            party_list          = ["(YSRCP) Yuvajana Sramika Rythu Congress Party", "(TDP) Telugu Desam Party", "(JP) JanaSena Party", "(INC) Indian National Congress","(BJP) Bharatiya Janata Party", "(AIMIM) All India Majlis-E-Ittehadul Muslimeen", "(TRS) Telangana Rashtra Samithi", "(BSP) Bahujan Samaj Party","(CPI(M)) Communist Party of India (Marxist)","(AAP) Aam AAdmi Party","(NOTA) None of the above","(CPI) Communist Party of India","(IND) Independent Candidate","Other Political Party" ]
            party_list_new      = [('YSRCP', 'Yuvajana Sramika Rythu Congress Party', '(YSRCP) Yuvajana Sramika Rythu Congress Party'),
                                   ('TDP', 'Telugu Desam Party', '(TDP) Telugu Desam Party'),
                                   ('JP', 'JanaSena Party', '(JP) JanaSena Party'),
                                   ('INC', 'Indian National Congress', '(INC) Indian National Congress'),
                                   ('BJP', 'Bharatiya Janata Party', '(BJP) Bharatiya Janata Party'),
                                   ('AIMIM', 'All India Majlis-E-Ittehadul Muslimeen', '(AIMIM) All India Majlis-E-Ittehadul Muslimeen'),
                                   ('TRS', 'Telangana Rashtra Samithi', '(TRS) Telangana Rashtra Samithi'),
                                   ('BSP', 'Bahujan Samaj Party','(BSP) Bahujan Samaj Party'),
                                   ('CPI(M)', 'Communist Party of India (Marxist)', '(CPI(M)) Communist Party of India (Marxist)'),
                                   ('AAP','Aam Aadmi Party','(AAP) Aam Aadmi Party'),
                                   ('NOTA', 'None of the above', '(NOTA) None of the above'),
                                   ('CPI', 'Communist Party of India', '(CPI) Communist Party of India'),
                                   ('IND', 'Independent Candidate', '(IND) Independent Candidate'),
                                   ('Other', 'Other Political Party', 'Other Political Party')]
            
            age_ranges          = [ (18, 25, '18-25'), (26, 35, '26-35'), (36, 50, '36-50'), (51, 98, 'Over_50')]
            caste_list          = ["Muslim","Vadabalija","Kapu","Mala","Koppulavelamas","Yadava","Thurpukapus","Gavara","Yata","Other Caste"]
            religion_list       = ["Hindu","Sikh","Jain","Buddhism","Christian","Muslim","Other Religion"]
            category_list       = ["GENERAL","OBC","SC","ST"]
            gender_categories   = ['Male', 'Female']
            area_list           = ['Panchayat', 'Municipal']
            incomes_list        = ['Below 1000','1001 to 2000','2001 to 3000','3001 to 5000','5001 to 7500','7501 to 10000','10001 to 15000','15001 to 20000','20001 to 30000','30001 to 50000','Above 50000','Not earning']
            
            result_age = []
            result_gender = []
            result_caste = []
            result_religion = []
            result_category = []
            result_area = []
            result_income = []

            result_age_chart = []
            result_gender_chart = []
            result_caste_chart = []
            result_religion_chart = []
            result_category_chart = []
            result_area_chart = []
            result_income_chart = []
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)
            
            if 'booth_number' in request.data and request.data['booth_number']:
                booth_list = [ac.strip() for ac in request.data['booth_number'].split(',')]
                filter_object &= Q(booth_number__in=booth_list)

            filterValue = None
            if 'election_code' in request.data and request.data['election_code']:
                inp_key = request.data['election_code']
                if inp_key == '2024AE':
                    filterValue = 'party_vote_for_2024ae'
                elif inp_key == '2024GE':
                    filterValue = 'party_vote_for_2024ge'
                elif inp_key == '2019AE':
                    filterValue = 'party_voted_for_2019ae'
                elif inp_key == '2019GE':
                    filterValue = 'party_voted_for_2019ge'
                
            # all_party_vote_share = SurveyForm.objects.filter(filter_object).exclude(**{filterValue: 'Respondent Quit'}).values(filterValue).annotate(count=Count('id'))
            # vote_mapping = {entry[filterValue]: entry['count'] for entry in all_party_vote_share}

            #----------Age Wise----------------------------
            age_mapping = {}
            for start, end, label in age_ranges:
                data_in_range = SurveyForm.objects.filter(filter_object, age__range = (start, end) ,**{filterValue + "__in": party_list}).count()
                age_mapping[label] = data_in_range

            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for start, end, age_range in age_ranges:
                    fetchcount = SurveyForm.objects.filter(filter_object,**{filterValue: party_match, 'age__range': (start, end)}).count()
                    denominator = age_mapping.get(age_range, 0)
                    if fetchcount != 0 and denominator != 0:
                        party_data[age_range] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[age_range] = 0.00
                result_age.append(party_data)

            for start, end, age_range in age_ranges:
                category = {'category': age_range}
                for entry in result_age:
                    category[entry['party_code']] = entry[age_range]
                result_age_chart.append(category)

            #----------Gender Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('gender').annotate(count = Count('id'))
            gender_mapping   = {entry['gender']: entry['count'] for entry in fetch_queryset}
            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for entry in gender_categories:
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'gender': entry}).count()
                    denominator = gender_mapping.get(entry, 0)
                    if fetchcount != 0 and denominator != 0:
                        party_data[entry] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[entry] = 0.00
                result_gender.append(party_data)

            for gender in gender_categories:
                category = {'category': gender}
                for entry in result_gender:
                    category[entry['party_code']] = entry[gender]
                result_gender_chart.append(category)
            #----------Caste Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('caste').annotate(count = Count('id'))
            caste_mapping   = {entry['caste']: entry['count'] for entry in fetch_queryset}

            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for caste in caste_list: 
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'caste': caste}).count()
                    denominator = caste_mapping.get(caste, 0)
                    if fetchcount != 0 and denominator != 0:
                        party_data[caste] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[caste] = 0.00
                result_caste.append(party_data)

            for caste in caste_list:
                category = {'category': caste}
                for entry in result_caste:
                    category[entry['party_code']] = entry[caste]
                result_caste_chart.append(category)

            #----------Religion Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('religion').annotate(count = Count('id'))
            religion_mapping   = {entry['religion']: entry['count'] for entry in fetch_queryset}

            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for religion in religion_list:
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'religion': religion}).count()
                    denominator = religion_mapping.get(religion, 0)
                    if fetchcount != 0 and denominator != 0:
                        party_data[religion] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[religion] = 0.00
                result_religion.append(party_data)
            
            for religion in religion_list:
                category = {'category': religion}
                for entry in result_religion:
                    category[entry['party_code']] = entry[religion]
                result_religion_chart.append(category)
            
            #----------Category Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('social_category').annotate(count = Count('id'))
            social_category_mapping   = {entry['social_category']: entry['count'] for entry in fetch_queryset}

            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for category in category_list:
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'social_category': category}).count()
                    denominator = social_category_mapping.get(category, 0)
                    if fetchcount != 0 and denominator != 0:
                        party_data[category] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[category] = 0.00
                result_category.append(party_data)
            
            for cat in category_list:
                category = {'category': cat}
                for entry in result_category:
                    category[entry['party_code']] = entry[cat]
                result_category_chart.append(category)
            
            #----------Area Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('area').annotate(count = Count('id'))
            area_mapping   = {entry['area']: entry['count'] for entry in fetch_queryset}

            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for area in area_list:
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'area': area}).count()
                    denominator = area_mapping.get(area, 0)

                    if fetchcount != 0 and denominator != 0:
                        party_data[area] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[area] = 0.00
                result_area.append(party_data)
            
            for area in area_list:
                category = {'category': area}
                for entry in result_area:
                    category[entry['party_code']] = entry[area]
                result_area_chart.append(category)
            
            #----------Income Wise----------------------------
            fetch_queryset = SurveyForm.objects.filter(filter_object,**{filterValue + "__in": party_list}).values('monthly_income_family').annotate(count = Count('id'))
            area_mapping   = {entry['monthly_income_family']: entry['count'] for entry in fetch_queryset}
            
            for party_code, party, party_match in party_list_new:
                party_data = {'party': party, 'party_code': party_code}
                for income in incomes_list:
                    fetchcount = SurveyForm.objects.filter(filter_object, **{filterValue: party_match, 'monthly_income_family': income}).count()
                    denominator = area_mapping.get(income, 0)

                    if fetchcount != 0 and denominator != 0:
                        party_data[income] = round((fetchcount / denominator) * 100, 2)
                    else:
                        party_data[income] = 0.00
                result_income.append(party_data)
            
            for income in incomes_list:
                category = {'category': income}
                for entry in result_income:
                    category[entry['party_code']] = entry[income]
                result_income_chart.append(category)

            result_objects = [
                    {'tbl_name': 'age_wise_vote_share',      'tbl_data': result_age,        'chart_data': result_age_chart},
                    {'tbl_name': 'gender_wise_vote_share',   'tbl_data': result_gender,     'chart_data': result_gender_chart},
                    {'tbl_name': 'caste_wise_vote_share',    'tbl_data': result_caste,      'chart_data': result_caste_chart},
                    {'tbl_name': 'religion_wise_vote_share', 'tbl_data': result_religion,   'chart_data': result_religion_chart},
                    {'tbl_name': 'category_wise_vote_share', 'tbl_data': result_category,   'chart_data': result_category_chart},
                    {'tbl_name': 'area_wise_vote_share',     'tbl_data': result_area,       'chart_data': result_area_chart},
                    {'tbl_name': 'income_wise_vote_share',   'tbl_data': result_income,     'chart_data': result_income_chart}
                ]

            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['data']      = result_objects
            return Response(result, status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        
class GetRawProfiles(APIView):
    def post(self,request):
        result = {}
        result['valid'] = False
        result['status'] = 'NOK'
        result['result'] = {'message':'Unauthorized access','data':{}}

        filter_object = Q()
        if 'ac_name' in request.data and request.data['ac_name'] != '':
            ac_condition = Q(ac_name = request.data['ac_name'])
            filter_object &= ac_condition

        if 'booth_number' in request.data and request.data['booth_number'] != '':
            booth_condition = Q(booth_number = request.data['booth_number'])
            filter_object &= booth_condition 
        
        start_date  = request.data['start_date']
        end_date    = request.data['end_date']

        if start_date=="" and end_date =="":
            start_date= "2024-01-01"
            end_date=(datetime.today()).strftime('%Y-%m-%d')

        elif (start_date!="" and end_date==""):
            end_date=(datetime.today()).strftime('%Y-%m-%d')
        
        if not validatedate(start_date):
            result['valid']=True
            result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
    
        if not validatedate(end_date):
            result['valid']=True
            result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
            return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

        start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
        end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)

        filter_object  &= Q(created_at__range=(start_date_new, end_date_new))

        caste_list                  = ["Muslim","Vadabalija","Kapu","Mala","Koppulavelamas","Yadava","Thurpukapus","Gavara","Yata","Other Caste"]
        religion_list               = ["Hindu","Sikh","Jain","Buddhism","Christian","Muslim","Other Religion"]
        social_category_list        = ["GENERAL","OBC","SC","ST"]

        keys = {
            'religion' : religion_list,
            'caste' : caste_list,
            'social_category':social_category_list
        }
        final_response = []
        for key in keys:
            table ={}
            table['tbl_name'] = 'ReligionWise Distribution' if key == 'religion' else ('CasteWise Distribution' if key == 'caste' else 'Social Category Distribution')
            filter_kwargs = {f"{key}__in": keys[key]}
            total_responses = SurveyForm.objects.filter(filter_object,**filter_kwargs).count()
            tabledata =  SurveyForm.objects.values(key).filter(filter_object,**filter_kwargs).annotate(total_response_submitted=Count('id')).order_by('-total_response_submitted')
            tbl_data = formatprofiledata(tabledata,key,total_responses,keys[key])
            table['tbl_data'] = tbl_data
            final_response.append(table)

        
        result['status'] = 'OK'
        result['valid']  = True
        result['result']['message'] = 'Distribution data fetched successfully'
        result['result']['data']    = final_response
        return Response(result,status=status.HTTP_200_OK) 
    
def formatprofiledata(inputdata,key,total_responses,religion_list):
    tbl_data = []
    for data  in inputdata:
        temp_row = {}
        if (data[key] in religion_list):
            temp_row['row_name'] = data[key]
            temp_row['total_response_submitted']   = data['total_response_submitted']
            temp_row['response_percentage'] = str(round((data['total_response_submitted']*100)/total_responses,2)) + "%"
            tbl_data.append(temp_row)
    return tbl_data


class AllPartyVoteShare(APIView):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try: 
            filter_object = Q()

            party_list = ["(BSP) Bahujan Samaj Party", "(TDP) Telugu Desam Party", "(BJP) Bharatiya Janata Party", "(INC) Indian National Congress","(CPI(M)) Communist Party of India (Marxist)","(CPI) Communist Party of India","(IND) Independent Candidate","(AIMIM) All India Majlis-E-Ittehadul Muslimeen","(JP) JanaSena Party","(TRS) Telangana Rashtra Samithi","(YSRCP) Yuvajana Sramika Rythu Congress Party","Other Political Party"]
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            start_date  = request.data.get('start_date')
            end_date    = request.data.get('end_date')

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            # ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_list = [ac.strip() for ac in request.data['ac_name'].split(',')]
                filter_object &= Q(ac_name__in=ac_list)
            
            if 'booth_number' in request.data and request.data['booth_number']:
                booth_list = [ac.strip() for ac in request.data['booth_number'].split(',')]
                filter_object &= Q(booth_number__in=booth_list)

            filterValue_list = ['party_vote_for_2024ae', 'party_vote_for_2024ge', 'party_voted_for_2019ae', 'party_voted_for_2019ge']
            result_list = []
            for filterValue in filterValue_list :
                count = SurveyForm.objects.filter(filter_object).values(filterValue).exclude(**{filterValue: 'Respondent Quit'}).annotate(count=Count('id'))
                result_list.append({
                    filterValue : count,
                })
            

            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['data']      = result_list
            return Response(result, status=status.HTTP_200_OK)
        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        
class PartyVoteShare(APIView):
    # @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}
        try:
            party_list          = ["(YSRCP) Yuvajana Sramika Rythu Congress Party", "(TDP) Telugu Desam Party", "(JP) JanaSena Party", "(INC) Indian National Congress","(BJP) Bharatiya Janata Party", "(AIMIM) All India Majlis-E-Ittehadul Muslimeen", "(TRS) Telangana Rashtra Samithi", "(BSP) Bahujan Samaj Party","(CPI(M)) Communist Party of India (Marxist)","(AAP) Aam AAdmi Party","(NOTA) None of the above","(CPI) Communist Party of India","(IND) Independent Candidate","Other Political Party" ]
            filter_object = Q()
            start_date  = request.data['start_date']
            end_date    = request.data['end_date']
            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
            end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
            filter_object &= Q(created_at__range=(start_date_new, end_date_new))
            if 'ac_name' in request.data and request.data['ac_name'] != '':
                ac_condition = Q(ac_name=request.data['ac_name'])
                filter_object &= ac_condition
            if 'booth_number' in request.data and request.data['booth_number'] != '':
                booth_condition = Q(booth_number = request.data['booth_number'])
                filter_object &= booth_condition
            final_response = {}

            election_keys = ['party_vote_for_2024ae', 'party_vote_for_2024ge', 'party_voted_for_2019ae', 'party_voted_for_2019ge']
            for key in election_keys:
                votedata = SurveyForm.objects.filter(filter_object,party_vote_for_2024ae__in=party_list).values(key).annotate(vote_count=Count(id))
                formatted_data = format_partyshare_data(votedata,key)
                final_response[key] = formatted_data
            
            final_formatted_response = []
            votedata_2024ae_partywise = final_response['party_vote_for_2024ae']
            votedata_2024ge_partywise = final_response['party_vote_for_2024ge']
            votedata_2019ae_partywise = final_response['party_voted_for_2019ae']
            votedata_2019ge_partywise = final_response['party_voted_for_2019ge']
            
            for party in party_list:
                temp_obj = {}
                temp_obj['party_name'] = party 
                temp_obj['2024ae_vote_count'] = next((entry['votecount'] for entry in votedata_2024ae_partywise if entry['party'] == party), 0)
                temp_obj['2024ae_vote_percent'] = next((entry['vote_percent'] for entry in votedata_2024ae_partywise if entry['party'] == party), 0)
                temp_obj['2024ge_vote_count'] = next((entry['votecount'] for entry in votedata_2024ge_partywise if entry['party'] == party), 0)
                temp_obj['2024ge_vote_percent'] = next((entry['vote_percent'] for entry in votedata_2024ge_partywise if entry['party'] == party), 0)
                temp_obj['2019ae_vote_count'] = next((entry['votecount'] for entry in votedata_2019ae_partywise if entry['party'] == party), 0)
                temp_obj['2019ae_vote_percent'] = next((entry['vote_percent'] for entry in votedata_2019ae_partywise if entry['party'] == party), 0)
                temp_obj['2019ge_vote_count'] = next((entry['votecount'] for entry in votedata_2019ge_partywise if entry['party'] == party), 0)
                temp_obj['2019ge_vote_percent'] = next((entry['vote_percent'] for entry in votedata_2019ge_partywise if entry['party'] == party), 0)
                final_formatted_response.append(temp_obj)
                
            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['data']      = final_formatted_response
            return Response(result,status=status.HTTP_200_OK)

        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)

def format_partyshare_data(votedata,key):
    party_list          = ["(YSRCP) Yuvajana Sramika Rythu Congress Party", "(TDP) Telugu Desam Party", "(JP) JanaSena Party", "(INC) Indian National Congress","(BJP) Bharatiya Janata Party", "(AIMIM) All India Majlis-E-Ittehadul Muslimeen", "(TRS) Telangana Rashtra Samithi", "(BSP) Bahujan Samaj Party","(CPI(M)) Communist Party of India (Marxist)","(AAP) Aam AAdmi Party","(NOTA) None of the above","(CPI) Communist Party of India","(IND) Independent Candidate","Other Political Party" ]
    total_vote = 0
    for x in votedata:
        if x[key] in party_list:
            total_vote += x['vote_count']
    res = []
    # print(key,votedata,total_vote)
    for party in party_list:
        temp = {}
        temp['party'] = party
        temp['votecount'] = next((entry['vote_count'] for entry in votedata if entry[key] == party), 0)
        temp['vote_percent'] = '0%' if total_vote == 0 else str(round((next((entry['vote_count'] for entry in votedata if entry[key] == party), 0) * 100/total_vote),2)) + '%'
        res.append(temp)
    return res

class ACWiseForms(APIView):
    @handle_auth_exceptions
    def post (self, request) :
        result={}
        result['status']  = "NOK"
        result['valid']   = False
        result['result']  = {"message":"Unauthorized access","data":[]}
        try:
            filter_object = Q()

            start_date  = request.data['start_date']
            end_date    = request.data['end_date']

            if start_date=="" and end_date =="":
                start_date= "2024-02-22"
                end_date=(datetime.today()).strftime('%Y-%m-%d')

            elif (start_date!="" and end_date==""):
               end_date=(datetime.today()).strftime('%Y-%m-%d')

            if not validatedate(start_date):
                result['valid']=True
                result['result']['message']="Invalid start date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)
        
            if not validatedate(end_date):
                result['valid']=True
                result['result']['message']="Invalid end date format, valid format yyyy-mm-dd"
                return Response(result,status= status.HTTP_422_UNPROCESSABLE_ENTITY)

            if 'ac_name' in request.data and request.data['ac_name']:
                ac_condition = Q(ac_name=request.data['ac_name'])
                filter_object &= ac_condition

            result_data = []

            if 'flag_form' in request.data and request.data['flag_form'] == "total_cf": 
                start_date_new = datetime.strptime(start_date, '%Y-%m-%d')
                end_date_new = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
                filter_object &= Q(created_at__range=(start_date_new, end_date_new))

                result_data = SurveyForm.objects.filter(filter_object).values() 

            if 'flag_form' in request.data and request.data['flag_form'] == "today_cf":
                result_data = SurveyForm.objects.filter(filter_object, created_at__date = datetime.today()).values() 
            
            for entry in result_data:
                ordered_entry = {
                    'surveyor_id'   : entry.get('surveyor_id'),
                    'response_id'   : entry.get('id'),
                    'name'          : entry.get('name'),  
                    'phone_number'  : entry.get('phone_number'),  
                }
                
                entry['qc_status']   = "1" if entry.get('qc_status') else "0"
                entry['created_at']  = entry['created_at'].date()
                entry['updated_at']  = entry['updated_at'].date()

                pop_list = ['id', 'name', 'phone_number', 'surveyor_id', 'govt_hospital_rating', 
                            'education_facility_rating', 'roads_rating', 'electricity_rating', 'drainage_rating', 'house_voters_count', 
                            'member_voted_2019ae', 'primary_consider_vote', 'vote_decision_2019ae', 'impt_election_issue_2024ae', 'good_things_jagan_govt', 
                            'failure_jagan_govt', 'shd_current_mla_ticket_again','sure_vote_current_mla', 'most_populous_caste', 'monthly_income_family', 
                            'things_possess', 'house_type', 'id_response', 'other_most_populous_caste']
                
                for key in pop_list:
                    entry.pop(key, None)
                
                ordered_entry.update(entry)
                entry.clear()
                entry.update(ordered_entry)

            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['data']      = result_data
            return Response(result,status=status.HTTP_200_OK)

        except KeyError as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e :
            result['result']['message'] = "Error: due to " + str(e) 
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        
class Get_QC_Response(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            qc_response_id = request.data['response_id']
            ac_name = request.data['ac']
            start_date = request.data['start_date']
            end_date = request.data['end_date']

            if start_date == "" and end_date == "":
                start_date = "2024-02-15"
                end_date=(datetime.today()).strftime('%Y-%m-%d')
            end_date=(datetime.strptime(end_date,'%Y-%m-%d') +timedelta(days=1)).strftime('%Y-%m-%d')     
            data = QcSurveyForm.objects.filter(created_at__range=(start_date,end_date),response_id=qc_response_id).values()   
            
            
            if ac_name != "":
                data = data.filter(ac_name=ac_name)
                                   
            qc_response_data = data.values( 'name', 'phone_number', 'area', 'age','gender','occupation','other_occupation','has_voted_2019ae','party_voted_for_2019ae', 'other_party_voted_for_2019ae','party_voted_for_2019ge','other_party_voted_for_2019ge', 'party_vote_for_2024ae','other_party_vote_for_2024ae','second_party_vote_for_2024ae','other_second_party_vote_for_2024ae', 'reason_vote_2024ae', 'pref_tdp_jsp_2024ae', 'party_vote_for_2024ge','other_party_vote_for_2024ge', 'next_cm_2024ae','other_next_cm_2024ae', 'is_satisfied_jagan_govt','schemes_heard_jagan_govt',  'any_member_scheme_benefit', 'better_jagan_cbn', 'shd_jagan_another_chance','is_satisfies_mla','party_can_win','other_party_can_win', 'caste','other_caste','social_category','religion','other_religion',  
 
            'response_id__name', 'response_id__phone_number', 'response_id__area', 'response_id__age', 'response_id__gender', 'response_id__occupation', 'response_id__other_occupation',  'response_id__has_voted_2019ae', 'response_id__party_voted_for_2019ae', 'response_id__other_party_voted_for_2019ae', 'response_id__party_voted_for_2019ge', 'response_id__other_party_voted_for_2019ge', 'response_id__party_vote_for_2024ae', 'response_id__other_party_vote_for_2024ae', 'response_id__second_party_vote_for_2024ae', 'response_id__other_second_party_vote_for_2024ae', 'response_id__reason_vote_2024ae', 'response_id__pref_tdp_jsp_2024ae', 'response_id__party_vote_for_2024ge', 'response_id__other_party_vote_for_2024ge', 'response_id__next_cm_2024ae', 'response_id__other_next_cm_2024ae', 'response_id__is_satisfied_jagan_govt', 'response_id__schemes_heard_jagan_govt', 'response_id__any_member_scheme_benefit', 'response_id__better_jagan_cbn', 'response_id__shd_jagan_another_chance', 'response_id__is_satisfies_mla', 'response_id__party_can_win', 'response_id__other_party_can_win', 'response_id__caste', 'response_id__other_caste', 'response_id__social_category', 'response_id__religion', 'response_id__other_religion'
            )
                                           
        
            qc_info = data.values('id', 'response_id__surveyor','gender', 'qc_agent_id_id', 'ac_name', 'created_at','recorded_audio')
            
            data2 = data.values_list('response_id',flat=True)
            match_que = AgentQcResponseMatch.objects.filter(response_id__in=data2).values()
           
            question_list=['age','area', 'occupation', 'has_voted_2019ae', 'party_voted_for_2019ae', 'party_voted_for_2019ge', 'party_vote_for_2024ae', 'second_party_vote_for_2024ae', 'pref_tdp_jsp_2024ae', 'party_vote_for_2024ge', 'next_cm_2024ae', 'is_satisfied_jagan_govt', 'better_jagan_cbn', 'shd_jagan_another_chance', 'is_satisfies_mla', 'party_can_win', 'caste', 'social_category', 'religion']
            question = []
            
            total_questions=35
            obj_question = 20
            for response in qc_response_data:
                res = {'qc_id':0,'agent_id':0,'date':0,'recording':0,'response_analysis':0}
                for k in response:
                    k2 = "response_id__"+k
                    if not k.startswith('response_id__') :
                        res[k] = [{
                                    "agent_response": 0,
                                    "qc_response": response[k],
                                    "match": "others"
                                }]
                    if k in res.keys():
                      res[k][0]['agent_response']=response[k2]

                question.append(res)
    
            for val in range(len(question)):
                question[val]['qc_id'] = qc_info[val]['qc_agent_id_id']
                question[val]['agent_id'] = qc_info[val]['response_id__surveyor']
                question[val]['date'] = qc_info[val]['created_at'].date()
                question[val]['recording'] = qc_info[val]['recorded_audio']
                question[val]['response_analysis'] = [ {
                                 "Total_Questions": total_questions,
                                 "Objective_Questions": obj_question,
                                 "Matched_Questions": match_que[val]['match_count'],
                                 "Unmatched_Questions": obj_question - int(match_que[val]['match_count'])
                             }]
                for i in question_list:
                    new_key = i + '_match'
                    question[val][i][0]['match'] = match_que[val][new_key]
        
            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data fetched successfully"
            result['result']['count'] = len(res)
            result['result']['data'] = question
            return Response(result, status=status.HTTP_200_OK)
        
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
