from django.apps import AppConfig


class FaqAndTutorialConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'faq_and_tutorial'
