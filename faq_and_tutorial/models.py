from django.db import models

# Create your models here.
class Faq(models.Model):
    question = models.TextField(default=None, null=True, blank=True)
    answer   = models.TextField(default=None, null=True, blank=True)