from django.shortcuts import render
from rest_framework.views import APIView
from .models import *
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
class GetFaqs(APIView):
    def get(self, request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}

        try:
            data = Faq.objects.all().values()
            result['status']    = 'OK'
            result['valid']     =  True
            result['result']['message'] = "Ac wise summary fetched successfully."
            result['result']['data']    = data

            return Response(result,status = status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)