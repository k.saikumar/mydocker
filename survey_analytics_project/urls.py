"""survey_analytics_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include ('users.urls')),
    path('qc/', include ('qc.urls')),

    path('field_summary/',include('agent_response.urls')),
    path('faq_and_tutorial/',include('faq_and_tutorial.urls')),
    path('notifications/',include('notifications.urls')),
    path('deficit_analysis/',include('deficit_analysis.urls')),
    path('rawdata_analysis/',include("rawdataanalysis.urls"))
]
