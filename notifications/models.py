from django.db import models
from agent_response.models import User
# Create your models here.
class Notification(models.Model):
    sender_id                    = models.ForeignKey(User,on_delete=models.CASCADE)
    sender_role                  = models.IntegerField(blank=True, null=True, default=1)
    notif_to                     = models.CharField(max_length=150, blank=True, null=True)
    notif_type                   = models.CharField(max_length=150, blank=True, null=True)
    notif_title                  = models.CharField(max_length=150, blank=True, null=True)
    notif_attachment             = models.CharField(max_length=150, blank=True, null=True)
    notif_attachment_type        = models.CharField(max_length=150, blank=True, null=True)
    notif_timestamp              = models.DateTimeField(blank=True, null=True)
    

    class Meta:
        managed = True
        db_table = 'tbl_notifications'





class UserAttendance(models.Model):
    user_id = models.PositiveIntegerField()
    punch_time = models.DateTimeField(null=True, blank=True)
    punch_status = models.IntegerField(default=0) 
    longitude = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)

    class Meta:
        managed = False  
        db_table = 'user_logs'

