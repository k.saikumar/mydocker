from django.urls import path
from .views import *

urlpatterns = [
    path('create_notification/', CreateNotification.as_view()),
    path('get_notification/', GetNotification.as_view()),
    path('update_notification/', UpdateNotification.as_view()),
    
    path('get_users_attendance/',Get_Users_Log.as_view()),
    
]