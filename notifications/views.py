from django.shortcuts import get_object_or_404, render
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from users.views import *
from .models import *
from agent_response.models import *
# Create your views here.
class CreateNotification(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            sender_id               = request.data['sender_id']
            sender_role             = request.data['sender_role']
            notif_to                = request.data['notif_to']
            notif_title             = request.data['notif_title']
            notif_attachment        = request.data['notif_attachment']
            notif_attachment_type   = request.data['notif_attachment_type']
            notif_type              = request.data['notif_type']
            notif_timestamp         = datetime.now()

            sender_user = get_object_or_404(User, id=sender_id)


            data = {
                "sender_id" : sender_user,
                "sender_role": sender_role,
                "notif_to": notif_to,
                "notif_title": notif_title,
                "notif_attachment" :  notif_attachment,
                "notif_attachment_type" : notif_attachment_type,
                "notif_timestamp" : notif_timestamp,
                "notif_type" : notif_type
            }

            Notification.objects.create(**data)

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data created successfully"

            return Response(result, status=status.HTTP_200_OK)
            
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetNotification(APIView, PageNumberPagination):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            # data = Notification.objects.filter().values()
            data = Notification.objects.order_by('-notif_timestamp').values()

            for response in data:
                response['notif_timestamp'] = response['notif_timestamp'].strftime("%Y-%m-%d %I:%M %p")

            paginated_data = self.paginate_queryset(data, request, view=self)
            paginated_data = self.get_paginated_response(paginated_data).data
            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['next']      = paginated_data['next']
            result['result']['previous']  = paginated_data['previous']
            result['result']['count']     = paginated_data['count']
            result['result']['data']      = paginated_data['results']


            return Response(result, status=status.HTTP_200_OK)
            
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UpdateNotification(APIView):
    @handle_auth_exceptions
    def post(self, request):
        result={}
        result['status']="NOK"
        result['valid']=False
        result['result']={"message":"Unauthorized access","data":[]}

        try:
            notif_id = request.data['notif_id']
            sender_id = request.data['sender_id']
            sender_role = request.data['sender_role']
            notif_to = request.data['notif_to']
            notif_title = request.data['notif_title']
            notif_attachment = request.data['notif_attachment']
            notif_attachment_type = request.data['notif_attachment_type']
            notif_type = request.data['notif_type']

            sender_user = get_object_or_404(User, id=sender_id)



            notif_data = Notification.objects.filter(id = notif_id).first()

            notif_data.sender_id = sender_user
            notif_data.sender_role = sender_role
            notif_data.notif_to = notif_to
            notif_data.notif_title = notif_title
            notif_data.notif_attachment = notif_attachment
            notif_data.notif_attachment_type = notif_attachment_type
            notif_data.notif_type = notif_type

            notif_data.save()

            result['status'] = "OK"
            result['valid'] = True
            result['result']['message'] = "Data updated successfully"

            return Response(result, status=status.HTTP_200_OK)
            
        except Exception as e :
            result['result']['message'] = str(e) 
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def processed_attendance_for_day(current_date, user_attendance, user_ids):
    serialized_data = []
    all_users = User.objects.filter(id__in=user_ids).values_list('id', 'fullname','role','ac_name')
    attendance_for_day = user_attendance.filter(user_id__in =user_ids, punch_time__date=current_date).values('id','user_id','punch_time','punch_status').order_by('-punch_time')   
    today_ids = list(set(attendance_for_day.values_list('user_id', flat=True)))
    today_agents = all_users.filter(id__in =today_ids )   
    absent_users = [user_id for user_id in user_ids if user_id not in today_ids]

    if attendance_for_day.exists():
        try:        
            # print("attendance_for_day",attendance_for_day)    
            day_attendance = attendance_for_day.order_by('-punch_time')
            for today_agent in today_ids:
                agent_details = today_agents.filter(id = today_agent).first()
                attendance_for_user = day_attendance.filter(user_id=today_agent).values('user_id','punch_time','punch_status')
                # print(f"attendance_for_user {attendance_for_user}")
                
                total_hours = None
                in_time, out_time = [],[]
                for entry in attendance_for_user:
                    punch_time = entry['punch_time']
                    if entry['punch_status'] == 0:    
                        in_time.append(punch_time)                            
                    else: 
                        out_time.append(punch_time)   

                in_time = sorted(in_time) 
                out_time = sorted(out_time)
               
                first_clock_in = in_time[0] if in_time else None
                last_clock_out = out_time[-1] if out_time else None
                
                if first_clock_in and last_clock_out:                       
                    total_duration = last_clock_out-first_clock_in

                    first_clock_in = first_clock_in.strftime("%Y-%m-%d %H:%M:%S")
                    last_clock_out = last_clock_out.strftime("%Y-%m-%d %H:%M:%S")
                    if total_duration.days<0:                       
                        total_hours = "00:00:00"
                    else:                       
                        total_hours, remainder = divmod(total_duration.total_seconds(), 3600)
                        total_minutes, total_seconds = divmod(remainder, 60)
                        total_hours = "{:02}:{:02}:{:02}".format(int(total_hours), int(total_minutes), int(total_seconds))

                serialized_data.append({
                        'date': current_date.strftime("%A, %B %d, %Y"),
                        'full_name': agent_details[1],
                        'id_type' : agent_details[2],
                        'ac_name' :agent_details[3],
                        'agent_id': today_agent,                 
                        'status': 'present',
                        'clock_in': first_clock_in,
                        # 'clock_out': clock_out_time,
                        'clock_out': last_clock_out if last_clock_out else "missed clock out",

                        'total_hours': total_hours,
                    
                    })

        except UserAttendance.DoesNotExist:
            pass
       
        else:
            
            for user in   absent_users:   
                agent_details = all_users.filter(id = user).first()
                # print("agent details",agent_details)     
                serialized_data.append({
                    'date': current_date.strftime("%A, %B %d, %Y"),
                    'full_name': agent_details[1],
                    'id_type' : agent_details[2],
                    'ac_name' :agent_details[3],
                    'agent_id': user,              
                    'status': 'absent',
                    'clock_in': None,
                    'clock_out': None,
                    'total_hours': None,
                })
    return serialized_data 

class Get_Users_Log(APIView,PageNumberPagination):
    @handle_auth_exceptions
    def post(self,request):
        result = {}
        result['status'] =  'NOK'
        result['valid']  =  False
        result['result'] = {"message":"Unauthorized access","data" :{}}
        try:
            user_data           =   request.user_data 
            if not user_data:
                result['result']['message']="Token expired. Please login again"
                return Response(result,status=status.HTTP_401_UNAUTHORIZED)
            
            filter_object = Q()
            if 'ac_name' in request.data and request.data['ac_name']:
                ac =  request.data['ac_name'].strip()
                filter_object &= Q(ac_name__icontains=ac)

            users_in_ac = User.objects.filter(filter_object).values()
            user_ids = [user['id'] for user in users_in_ac]
     
            end_date  = request.data.get('date')
            serialized_data =[]
            if end_date:
                if not validatedate(end_date):
                    result['valid'] = True
                    result['result']['message'] = "Invalid start date format, valid format yyyy-mm-dd"
                    return Response(result, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

                end_date = datetime.strptime(end_date, '%Y-%m-%d')
                current_date = end_date - timedelta(days=0)
                user_attendance = UserAttendance.objects.filter(user_id__in=user_ids,punch_time__date= current_date).order_by('-punch_time') 
                # print("user_attendance",user_attendance.values())  
                serialized_data = processed_attendance_for_day(current_date, user_attendance, user_ids)               
            # else:            
            #     end_date = timezone.now().date()            
            #     start_date = end_date - timedelta(days=29)
            #     user_attendance = UserAttendance.objects.filter(Q(user_id__in = user_ids),Q(punch_time__date__range=[start_date, end_date])).order_by('-punch_time')
                
            #     for day in range(30):
            #         current_date = end_date - timedelta(days=day)
            #         attendance_for_day = user_attendance.filter(punch_time__date=current_date)
            #         serialized_data.append(processed_attendance_for_day(current_date, attendance_for_day, user_ids))

            paginated_data = self.paginate_queryset(serialized_data, request, view=self)
            paginated_data = self.get_paginated_response(paginated_data).data
            result['status']              = "OK"
            result['valid']               = True
            result['result']['message']   = "Data fetched successfully"
            result['result']['next']      = paginated_data['next']
            result['result']['previous']  = paginated_data['previous']
            result['result']['count']     = paginated_data['count']
            result['result']['data']      = paginated_data['results']
            return Response(result, status=status.HTTP_200_OK)

            # result['status'] = "OK"
            # result['valid'] = True
            # result['total_count'] = len(serialized_data)
            # result['result']['message'] = "User attendance fetched successfully"
            # result['result']['data'] = serialized_data
            # return Response(result,status = status.HTTP_200_OK)
        except Exception as e:
            result['result']['message'] = f"Error fetching data: {str(e)}"
            return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
     
               